package com.project.ali.myapplication.pages;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.project.ali.myapplication.R;
//import com.project.ali.myapplication.app.AppConfig;

/**
 * Created by Alistair
 */
public class EnglandTeamInfo extends AppCompatActivity    {
    String Team_name= "";
    ListView listView;

    Button back;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.england_team_information);


        //Finding the list from the xml page
        listView = (ListView) findViewById(R.id.listEngland);

        //Array to hold values which will be displayed on the xml page
        String[] values = new String[]{
                "Goalkeeper: ",
                "Goalkeeper: ",
                "Defender: ",
                "Defender: ",
                "Defender: ",
                "Defender: ",
                "Defender: ",
                "Midfielder: ",
                "Midfielder: ",
                "Midfielder: ",
                "Midfielder: ",
                "Forwards: ",
                "Forwards: ",
                "Forwards: ",
                "Forwards: ",
                "Forwards: ",
                "Forwards: ",
                "Forwards: "

        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);


        // Listview
        listView.setAdapter(adapter);

        //OnItemClickListener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition = position;

                //Get the value of the item clicked
                String itemValue = (String) listView.getItemAtPosition(position);

                //Toast to show information clicked.
                //No real purpose but to highlight info
                Toast.makeText(getApplicationContext(),
                        itemValue, Toast.LENGTH_SHORT)
                        .show();


            }

        });
    }


    }
