package com.project.ali.myapplication.pages;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.ali.myapplication.R;

/**
 * Created by Alistair
 */
public class TicketsFragment extends Fragment implements View.OnClickListener {
    ImageView Email;
    ImageView Link;
    ImageView tktMaster;
    View myView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //instansiate the view
        myView = inflater.inflate(R.layout.tickets_screen, container,false);

        //Call ticket master method
        TicketMaster();

        //Call email method
        emailClick();

        //Call on back pressed method
        onBackPressed();

        //Call NI
        NIClick();

        //Call eurosport
        EurosportClick();

        //Call Addidas
        AddidasClick();


        return myView;
    }

    /**
     * Method to take the user to Ticketmaster
     */
    public void TicketMaster(){
          ImageView  tktMaster = (ImageView) myView.findViewById(R.id.TicketMasterLinkTickets);
            tktMaster.setOnClickListener(this);
        }



    /**
     * Method to take the user to their email client
     */
    public void emailClick() {

        ImageView Email = (ImageView) myView.findViewById(R.id.EmailIconImage);
        Email.setOnClickListener(this);

    }

    /**
     * Method to take the user to Northern Ireland page
     */
    public void NIClick() {

        Link = (ImageView) myView.findViewById(R.id.NorthernIrelandCrestBottomBanner);
        Link.setOnClickListener(this);

    }

    /**
     * Method to take the user to Addidas page
     */
    public void AddidasClick() {

        Link = (ImageView) myView.findViewById(R.id.AddidasImage);
        Link.setOnClickListener(this);

    }

    /**
     * Method to take the user to Eurosport page
     */
    public void EurosportClick() {

        Link = (ImageView) myView.findViewById(R.id.EuroSportImage);
        Link.setOnClickListener(this);

    }
    /**
     * Overrule back button on Android
     */
    public void onBackPressed() {

    }


    /**
     * Switch for user clicking link to ticketmaster
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.TicketMasterLinkTickets:
                //Ticketmaster link will need updated to take to direct page
                Intent ticket = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.ticketmaster.co.uk/"));
                startActivity(ticket);
                break;
            case R.id.EmailIconImage:
                Intent Email = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:u19eurostickets@outlook.com?subject=Ticket Enquiry&body= Please enter your enquiry and supply relevant contact details"));
                startActivity(Email);
                break;
            case R.id.AddidasImage:
            Intent addidas = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.adidas.co.uk/"));
            startActivity(addidas);
            break;
            case R.id.EuroSportImage:
                Intent euro = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.eurosport.co.uk/"));
                startActivity(euro);
                break;
            case R.id.NorthernIrelandCrestBottomBanner:
                Intent ni = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.irishfa.com/"));
                startActivity(ni);
                break;
            default:
                break;
        }
    }

}
