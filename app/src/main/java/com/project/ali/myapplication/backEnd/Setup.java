//package com.project.ali.myapplication.backEnd;
//
//import android.content.ContentValues;
//import android.content.Context;
//import android.database.Cursor;
//import android.database.sqlite.SQLiteDatabase;
//import android.database.sqlite.SQLiteOpenHelper;
//
//
//import com.project.ali.myapplication.middleTier.User;
//
//import java.util.LinkedList;
//import java.util.List;
//
///**
// * Created by Alistair on 29-Mar-17. Code taken from online source
// */
//public class Setup extends SQLiteOpenHelper {
//
//    public static final String DATABASE_NAME = "euro.db";
//    public static final String USER_TABLE_NAME = "user_table";
//    private static final String USER_COL1 = "firstName";
//    private static final String USER_COL2 = "lastName";
//    private static final String USER_COL3 = "email";
//    private static final String USER_COL4 = "username";
//    private static final String USER_COL5 = "password";
//   // private static final String USER_COL6 = "sex";
//    //private static final String USER_COL7 = "firstTime";
//    private static final String USER_COLUMNS[] = {"first_name","last_name","email","username","password"};
//    //, "sex", "first_time"};
//
//
//
//    public Setup(Context context) {
//
//        super(context, DATABASE_NAME, null, 1);
//    }
//
//    @Override
//    public void onCreate(SQLiteDatabase db) {
//
//        // creating user table
//        db.execSQL("create table " + USER_TABLE_NAME + " (firstname VARCHAR,lastname VARCHAR, email VARCHAR, username VARCHAR, password VARCHAR," +
////                "sex VARCHAR, VARCHAR firsttime, " +
//                "PRIMARY KEY (email))");
//
//
//    }
//
//    @Override
//    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//
//        db.execSQL("DROP TABLE IF EXISTS "+USER_TABLE_NAME);
//
//
//        onCreate(db);
//
//    }
//
//    public void createUser(User user){
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put(USER_COL1, user.getFirstname());
//        values.put(USER_COL2, user.getLastname());
//        values.put(USER_COL3, user.getEmail());
//        values.put(USER_COL4, user.getUsername());
//        values.put(USER_COL5, user.getPassword());
//        //values.put(USER_COL6, user.getSex());
//        //values.put(USER_COL7, user.getFirstTime());
//    }
//
//    public User readUser(String email){
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.query(USER_TABLE_NAME, USER_COLUMNS, " email = ?", new String[]{String.valueOf(email)}, null, null, null);
//        if (cursor!=null)
//            cursor.moveToFirst();
//
//        User user = new User(null, null, null, null ,null);
//        //,null,null);
//        user.setFirstname(cursor.getString(0));
//        user.setLastname(cursor.getString(1));
//        user.setEmail(cursor.getString(2));
//        user.setUsername(cursor.getString(3));
//        user.setPassword(cursor.getString(4));
//       // user.setSex(cursor.getString(5));
//      //  user.setFirstTime(cursor.getString(6));
//        return user;
//    }
//
//    public Cursor getAllData(){
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        Cursor result = db.rawQuery("SELECT * FROM "+USER_TABLE_NAME,null);
//        return result;
//    }
//
//    public List getAllUsers(){
//        List users = new LinkedList();
//
//        // select user query
//        String query = "SELECT * FROM "+USER_TABLE_NAME;
//
//        // get reference of the DB
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(query,null);
//
//        // parse all results
//        User user = null;
//        if(cursor.moveToFirst()){
//            do{
//                user = new User(null, null, null, null, null);
//                //, null, null);
//                user.setFirstname(cursor.getString(0));
//                user.setLastname(cursor.getString(1));
//                user.setEmail(cursor.getString(2));
//                user.setUsername(cursor.getString(3));
//                user.setPassword(cursor.getString(4));
//          //      user.setSex(cursor.getString(5));
//            //    user.setFirstTime(cursor.getString(6));
//
//                users.add(user);
//            } while (cursor.moveToNext());
//        }
//
//        return users;
//    }
//
//    public int updateUser(User user){
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put("firstname",user.getFirstname());
//        values.put("lastname",user.getLastname());
//        values.put("email",user.getEmail());
//        values.put("username",user.getUsername());
//        values.put("password",user.getPassword());
////        values.put("sex", user.getSex());
////        values.put("firsttime", user.getFirstTime());
//
//        int i = db.update(USER_TABLE_NAME,values,USER_COL3 + " = ?",new String[]{String.valueOf(user.getEmail())});
//        db.close();
//        return i;
//    }
//
//    public void deleteUser(User user){
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.delete(USER_TABLE_NAME,USER_COL3 + " = ?", new String[] {String.valueOf(user.getEmail())});
//        db.close();
//    }
//
//
//
//}
