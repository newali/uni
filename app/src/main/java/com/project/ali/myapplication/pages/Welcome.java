package com.project.ali.myapplication.pages;


import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.project.ali.myapplication.R;
import com.project.ali.myapplication.activities.LoginActivity;
import com.project.ali.myapplication.activities.RegisterActivity;
import com.project.ali.myapplication.instagram.*;
//import com.project.ali.myapplication.backEnd.Setup;
//import com.project.ali.myapplication.middleTier.User;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;
import net.hockeyapp.android.metrics.MetricsManager;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Alistair
 */
public class Welcome extends AppCompatActivity {

    private AdView mAdView;
    private Button btnFullscreenAd;

    // declaring database from dataAccess
//    Setup eurodb = new Setup(this);
//    List<User> list;
//    List<String>userEmails;
//    ArrayAdapter myAdapter;

    /**
     * Main method for Welcome
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MetricsManager.register(this, getApplication());
        //Screen set to welcome screen
        setContentView(R.layout.welcome_screen);

//        list = eurodb.getAllUsers();
//        List<String> listEmail = new ArrayList<String>();
//
//        for (int i = 0; i < eurodb.getAllUsers().size(); i++){
//            listEmail.add(i, list.get(i).getEmail());
//        }



        // calling method required to write to database
     //   eurodb.getWritableDatabase();


        //Call the eurosport method
        eurosportClick();

        //Call the adidas method
        adidasClick();

        //Call the facebook method
        facebookClick();

        //Call the instagram method
        instagramClick();

        //Call the snapchat method
        snapchatClick();

        //Call the twitter method
        twitterClick();

        //Call the youtube method
        youtubeClick();

        //Call the email click
        emailClick();

        //Call the Ticket click method
        TicketClick();

        //Call the ad method
        ads();

        //Call the Northern Ireland clicked method
        Northern();

        //Call Register button
        Register();

        //Call login button
        Login();

        //Call French
        French();

        //Call English
        English();

        //Call French image
        frenchImage();

        //Call English image
        englishImage();

        //Call English text
        englishText();

        //Call French text
        frenchText();

        //Call French Switch
        frenchSwitch();

        //Call English Switch
        englishSwitch();

        //Call method to check for any app crashes
        checkForCrashes();

        //Call method to check for updates
        checkForUpdates();
    }

//    public void onItemClick(AdapterView arg0, View arg1, int arg2, long arg3){
//        Intent intent = new Intent (this,Register.class);
//        intent.putExtra("user",list.get(arg2).getEmail());
//        startActivityForResult(intent,1);
//    }

//    public void onActivityResult(int requestCode, int resultCode, Intent data){
//        super.onActivityResult(requestCode, resultCode,data);
//
//        // get all users again because something happened
//        list = eurodb.getAllUsers();
//        List<String> listEmail2 = new ArrayList<String>();
//
//        for (int i = 0; i <list.size(); i++){
//            listEmail2.add(i, list.get(i).getEmail());
//        }
//    }

    public void showMessage(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }


    /**
     * Using the login button and setting a click listener
     */
    public void Login() {

        Button loginBtn = (Button) findViewById(R.id.LoginButtonWelcome);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), LoginActivity.class);
                startActivity(intent);
            }
        });

    }

    /**
     * using the register button and setting a click listener.
     */
    public void Register() {
        Button registerBtn = (Button) findViewById(R.id.RegisterButtonWelcome);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), RegisterActivity.class);
                startActivity(i);
            }
        });
    }


    /**
     * Method to take the user to facebook
     */
    public void facebookClick() {

        ImageView reviews = (ImageView) findViewById(R.id.facebook_logo);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent facebook = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/UEFAWU192017/?pnref=story"));
                startActivity(facebook);
            }
        });
    }

    /**
     * Method to take the user to instagram
     */
    public void instagramClick() {

        ImageView reviews = (ImageView) findViewById(R.id.instagram_logo);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent instagram = new Intent(v.getContext(), com.project.ali.myapplication.instagram.MainActivity.class);
               // startActivity(intent);
             //   Intent instagram = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/?hl=en"));
                startActivity(instagram);
            }
        });
    }

    /**
     * Method to take the user to snapchat
     */
    public void snapchatClick() {

        ImageView reviews = (ImageView) findViewById(R.id.snapchat_logo);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent snapchat = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.snapchat.com/l/en-gb/"));
                startActivity(snapchat);
            }
        });
    }

    /**
     * Method to take the user to youtube
     */
    public void youtubeClick() {

        ImageView reviews = (ImageView) findViewById(R.id.youtube_logo);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent youtube = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/user/OfficialIrishFA"));
                startActivity(youtube);
            }
        });
    }

    /**
     * Method to take the user to their email client
     */
    public void emailClick() {

        ImageView reviews = (ImageView) findViewById(R.id.email_logo);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent email = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:U19Euros@outlook.com"));
                startActivity(email);
            }
        });
    }


    /**
     * Method to take the user to Eurosport
     */
    public void eurosportClick() {

        ImageView reviews = (ImageView) findViewById(R.id.EuroSportImage);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent eurosport = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.eurosport.co.uk/"));
                startActivity(eurosport);
            }
        });
    }

    /**
     * Method to take the user to Twitter
     */
    public void twitterClick() {

        ImageView reviews = (ImageView) findViewById(R.id.twitter_logo);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent twitter = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/OfficialIrishFA?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor"));
                startActivity(twitter);
            }
        });
    }

    /**
     * Method to take the user to Adidas
     */
    public void adidasClick() {

        ImageView reviews = (ImageView) findViewById(R.id.AddidasImage);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent adidas = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.adidas.co.uk/"));
                startActivity(adidas);
            }
        });
    }

    /**
     * Method to take user to Northern Ireland webpage
     */
    public void Northern() {
        ImageView reviews = (ImageView) findViewById(R.id.NorthernIrelandCrestBottomBanner);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent NI = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.irishfa.com/"));
                startActivity(NI);
            }
        });
    }

    /**
     * Method when user clicks buy tickets to be taken to ticketmaster
     */
    public void TicketClick() {

        TextView tkt = (TextView) findViewById(R.id.BuyTicketsWelcome);

        tkt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.ticketmaster.co.uk/"));
                startActivity(intent);
            }
        });
    }

    /**
     * Ad view method
     */
    public void ads() {
        MobileAds.initialize(getApplicationContext(), "ca-app-pub-5995906643043863/8800809830");
        //Finding the ad on the xml page and building it
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);


    }

    /**
     * Setting French Image as Visible
     */
    public void frenchImage() {
        ImageView fr = (ImageView) findViewById(R.id.LanguageChange);
        fr.setVisibility(View.VISIBLE);
    }

    /**
     * Setting English Image as Invisible
     */
    public void englishImage() {
        ImageView en = (ImageView) findViewById(R.id.LanguageChangeEnglish);
        en.setVisibility(View.INVISIBLE);
    }

    /**
     * Setting English Text as Visible
     */
    public void englishText() {
        TextView enTxt = (TextView) findViewById(R.id.WelcomeInfo);
        enTxt.setVisibility(View.VISIBLE);
    }

    /**
     * Setting French Text as Invisible
     */
    public void frenchText() {
        TextView enTxt = (TextView) findViewById(R.id.WelcomeInfoFrench);
        enTxt.setVisibility(View.INVISIBLE);
    }

    /**
     * Information on switching to French as Visible
     */
    public void frenchSwitch() {
        TextView enTxt = (TextView) findViewById(R.id.SwitchFrenchWelcome);
        enTxt.setVisibility(View.VISIBLE);
    }

    /**
     * Information on switching to English as Invisible
     */
    public void englishSwitch() {
        TextView enTxt = (TextView) findViewById(R.id.SwitchEnglishWelcome);
        enTxt.setVisibility(View.INVISIBLE);
    }

    /**
     * Method for changing language to French
     */
    public void French() {
        final ImageView French = (ImageView) findViewById(R.id.LanguageChange);
        final ImageView English = (ImageView) findViewById(R.id.LanguageChangeEnglish);
        final TextView EnglishLan = (TextView) findViewById(R.id.WelcomeInfo);
        final TextView FrenchLan = (TextView) findViewById(R.id.WelcomeInfoFrench);
        final TextView FrenchSwitch = (TextView) findViewById(R.id.SwitchFrenchWelcome);
        final TextView EnglishSwitch = (TextView) findViewById(R.id.SwitchEnglishWelcome);


        French.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                v.equals(French);
                if (v.equals(French)) {
                    French.setVisibility(View.INVISIBLE);
                    English.setVisibility(View.VISIBLE);
                    EnglishLan.setVisibility(View.INVISIBLE);
                    FrenchLan.setVisibility(View.VISIBLE);
                    FrenchSwitch.setVisibility(View.INVISIBLE);
                    EnglishSwitch.setVisibility(View.VISIBLE);


                    //Toast for language change to appear
                    Toast.makeText(getApplicationContext(), "Language changed to French", Toast.LENGTH_SHORT).show();


                    //English method called
                    English();
                }
            }
        });
    }


    /**
     * Method for changing language back to English
     */

    public void English() {
        final ImageView French = (ImageView) findViewById(R.id.LanguageChange);
        final ImageView English = (ImageView) findViewById(R.id.LanguageChangeEnglish);
        final TextView EnglishLan = (TextView) findViewById(R.id.WelcomeInfo);
        final TextView FrenchLan = (TextView) findViewById(R.id.WelcomeInfoFrench);
        final TextView FrenchSwitch = (TextView) findViewById(R.id.SwitchFrenchWelcome);
        final TextView EnglishSwitch = (TextView) findViewById(R.id.SwitchEnglishWelcome);

        English.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Setting the visibility of the images and text
                French.setVisibility(View.INVISIBLE);
                English.setVisibility(View.VISIBLE);
                EnglishLan.setVisibility(View.INVISIBLE);
                FrenchLan.setVisibility(View.VISIBLE);
                FrenchSwitch.setVisibility(View.INVISIBLE);
                EnglishSwitch.setVisibility(View.VISIBLE);

                //If the image is clicked changing the images and texts visibility
                v.equals(English);
                if (v.equals(English)) {
                    French.setVisibility(View.VISIBLE);
                    English.setVisibility(View.INVISIBLE);
                    EnglishLan.setVisibility(View.VISIBLE);
                    FrenchLan.setVisibility(View.INVISIBLE);
                    FrenchSwitch.setVisibility(View.VISIBLE);
                    EnglishSwitch.setVisibility(View.INVISIBLE);

                    //Toast for language change to appear
                    Toast.makeText(getApplicationContext(), "Language changed to English", Toast.LENGTH_SHORT).show();

                    //Calling French method
                    French();

                }
            }
        });
    }
    /**
     * Method linked to HockeyApp
     */
    private void checkForCrashes() {
        CrashManager.register(this);
    }

    /**
     * Check for any updates using the import from HockeyApp
     */
    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this);
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }


    @Override
    public void onPause() {
        super.onPause();
        unregisterManagers();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterManagers();
    }

}