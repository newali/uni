package com.project.ali.myapplication.pages;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.ali.myapplication.R;

/**
 * Created by Alistair
 */
public class DirectionsFragment extends Fragment implements View.OnClickListener {
    TextView Directions;
    ImageView Image;
    View myView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //instansiate the view
        myView = inflater.inflate(R.layout.user_directions, container, false);

        //Call method for user Directions
        uDirections();

        //Call method for eurosport image being clicked
        EuroSport();

        //Call method for addidas image being clicked
        Addidas();

        //Call method for NI image being clicked
        Northern();

        return myView;
    }

    /**
     * Directions method
     */
    public void uDirections() {
            Directions = (TextView) myView.findViewById(R.id.GetDirectionsLink);
            Directions.setOnClickListener(this);
    }


    /**
     * Method for eurosport
     */
    public void EuroSport() {
        Image = (ImageView) myView.findViewById(R.id.EuroSportImage);
       Image.setOnClickListener(this);
    }

    /**
     * Method for Addidas
     */
    public void Addidas() {
        Image = (ImageView) myView.findViewById(R.id.AddidasImage);
        Image.setOnClickListener(this);
    }

    /**
     * Method for nothern ireland image
     */
    public void Northern() {
        Image = (ImageView) myView.findViewById(R.id.NorthernIrelandCrestBottomBanner);
        Image.setOnClickListener(this);
    }

    /**
     * Switch for user clicking the knockout link
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.GetDirectionsLink:
                Intent link = new Intent(v.getContext(),UserDirections.class);
                startActivity(link);
                break;
            case R.id.EuroSportImage:
                Intent Euro = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.eurosport.co.uk/"));
                startActivity(Euro);
                break;
            case R.id.AddidasImage:
                Intent Addidas = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.adidas.co.uk/"));
                startActivity(Addidas);
                break;
            case R.id.NorthernIrelandCrestBottomBanner:
                Intent NI = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.irishfa.com/"));
                startActivity(NI);
                break;
            default:
                break;
        }
    }

    /**
     * method for back button
     */
    public void onBackPressed() {

    }
}
