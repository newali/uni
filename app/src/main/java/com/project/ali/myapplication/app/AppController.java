package com.project.ali.myapplication.app;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Taken from online source
 * Ravi Tamada
 * Android login and registration with php, mysql and sqlite
 * www.androidhive.info/2012/01/android-login-and-registration-with-php-mysql-and-sqlite/
 */
public class AppController extends Application{

    public static final String TAG=AppController.class.getSimpleName();

    private RequestQueue rqRequestQueue;

    private static AppController iInstance;

  @Override
    public void onCreate(){
        super.onCreate();
        iInstance=this;
    }

    /**
     * @return
     */
    public static synchronized AppController getInstance(){
    return iInstance;
    }
    public RequestQueue getRequestQueue(){
        if(rqRequestQueue == null){
            rqRequestQueue= Volley.newRequestQueue(getApplicationContext());
        }

        return rqRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag){
        req.setTag(TextUtils.isEmpty(tag)? TAG: tag);
        getRequestQueue(). add(req);
    }

    public <T> void addToRequestQueue(Request<T> req){
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag){
        if(rqRequestQueue !=null){
            rqRequestQueue.cancelAll(tag);
        }
    }

}
