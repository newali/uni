package com.project.ali.myapplication.pages;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.project.ali.myapplication.R;

/**
 * Created by Alistair
 */
public class HistoryFragment extends Fragment implements  View.OnClickListener{
    View myView;
    ImageView Link;
    Button Click;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //instansiate the view
        myView = inflater.inflate(R.layout.history, container, false);

        //Call golden players method
        GoldenPlayers();

        //Call past champions method
        PastChampions();

        //Call NI
        NIClick();

        //Call eurosport
        EurosportClick();

        //Call Addidas
        AddidasClick();

        return myView;
    }

    /*
     *Golden players method setting click listener
     */
    public void GoldenPlayers() {
        Click = (Button) myView.findViewById(R.id.GoldenPlayersButton);
        Click.setOnClickListener(this);
    }

    /*
    *Past champions method setting click listener
    */
    public void PastChampions() {
        Click = (Button) myView.findViewById(R.id.PastChampionsButton);
        Click.setOnClickListener(this);
    }

    /**
     * Method set on click listener
     */
    public void NIClick() {

        Link = (ImageView) myView.findViewById(R.id.NorthernIrelandCrestBottomBanner);
        Link.setOnClickListener(this);

    }

    /**
     * Method to take the user to Addidas page
     */
    public void AddidasClick() {

        Link = (ImageView) myView.findViewById(R.id.AddidasImage);
        Link.setOnClickListener(this);

    }

    /**
     * Method to take the user to Eurosport page
     */
    public void EurosportClick() {

        Link = (ImageView) myView.findViewById(R.id.EuroSportImage);
        Link.setOnClickListener(this);

    }

    /**
     * Switch for user clicking a button
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.GoldenPlayersButton:
                Intent Golden = new Intent(v.getContext(), GoldenPlayers.class);
                startActivity(Golden);
                break;
            case R.id.PastChampionsButton:
                Intent Champion = new Intent(v.getContext(), PastChampions.class);
                startActivity(Champion);
                break;
            case R.id.AddidasImage:
                Intent addidas = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.adidas.co.uk/"));
                startActivity(addidas);
                break;
            case R.id.EuroSportImage:
                Intent euro = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.eurosport.co.uk/"));
                startActivity(euro);
                break;
            case R.id.NorthernIrelandCrestBottomBanner:
                Intent ni = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.irishfa.com/"));
                startActivity(ni);
                break;
            default:
                break;
        }
    }

    /**
     * Method which doesnt work on Fragments
     */
    public void onBackPressed() {

    }
}
