package com.project.ali.myapplication.pages;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.ali.myapplication.R;
import com.project.ali.myapplication.pages.EnglandTeamInfo;
import com.project.ali.myapplication.pages.FranceTeamInfo;
import com.project.ali.myapplication.pages.GermanyTeamInfo;
import com.project.ali.myapplication.pages.LiveScores;
import com.project.ali.myapplication.pages.NITeamInfo;

/**
 * Created by Alistair
 */
public class TeamInformationFragment extends Fragment implements View.OnClickListener {
    ImageView teams;
    ImageView Link;
    TextView team;
    View myView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //instansiate the view
        myView = inflater.inflate(R.layout.team_information_screen, container,false);

        //Call the image click methods
        NorthernIreland();
        England();
        Germany();
        France();
        Spain();
        Ukraine();

        //Call text click method
        Team();

        //Call NI method
        NIClick();

        //Call eurosport method
        EurosportClick();

        //Call Addidas method
        AddidasClick();

        //Call method onBackPressed
        onBackPressed();

        return myView;
    }
    /**
     * Method for clicking Northern Ireland image
     */
    public void NorthernIreland(){
        teams = (ImageView) myView.findViewById(R.id.NICrest);
        teams.setOnClickListener(this);
    }
    /**
     * Method for clicking England image
     */
    public void England(){
        teams = (ImageView) myView.findViewById(R.id.EnglandCrest);
        teams.setOnClickListener(this);
    }
    /**
     * Method for clicking Germany image
     */
    public void Germany(){
        teams = (ImageView) myView.findViewById(R.id.GermanyCrest);
        teams.setOnClickListener(this);
    }

    /**
     * Method for clicking France image
     */
    public void France(){
        teams = (ImageView) myView.findViewById(R.id.FranceCrest);
        teams.setOnClickListener(this);
    }

    /**
     * Method for clicking Spain image
     */
    public void Spain(){
        teams = (ImageView) myView.findViewById(R.id.SpanishCrest);
        teams.setOnClickListener(this);
    }

    /**
     * Method for clicking Ukraine image
     */
    public void Ukraine(){
        teams = (ImageView) myView.findViewById(R.id.UkraineCrest);
        teams.setOnClickListener(this);
    }


    /**
     * Method for clicking statistic text
     */
    public void Team(){
        team = (TextView) myView.findViewById(R.id.StatLink);
        team.setOnClickListener(this);
    }

    /**
     * Method to take the user to Northern Ireland page
     */
    public void NIClick() {

         Link = (ImageView) myView.findViewById(R.id.NorthernIrelandCrestBottomBanner);
        Link.setOnClickListener(this);

    }

    /**
     * Method to take the user to Addidas page
     */
    public void AddidasClick() {

       Link = (ImageView) myView.findViewById(R.id.AddidasImage);
        Link.setOnClickListener(this);

    }

    /**
     * Method to take the user to Eurosport page
     */
    public void EurosportClick() {

        Link = (ImageView) myView.findViewById(R.id.EuroSportImage);
        Link.setOnClickListener(this);

    }

    /**
     * Switch for user clicking an image
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.NICrest:
                Intent NI = new Intent(v.getContext(), NITeamInfo.class);
                startActivity(NI);
                break;
            case R.id.EnglandCrest:
                Intent  England= new Intent(v.getContext(), EnglandTeamInfo.class);
                startActivity(England);
                break;
            case R.id.GermanyCrest:
                Intent Germany = new Intent(v.getContext(), GermanyTeamInfo.class);
                startActivity(Germany);
                break;
            case R.id.FranceCrest:
                Intent France = new Intent(v.getContext(), FranceTeamInfo.class);
                startActivity(France);
                break;
            case R.id.SpanishCrest:
                Intent Spain = new Intent(v.getContext(), SpainTeamInfo.class);
                startActivity(Spain);
                break;
            case R.id.UkraineCrest:
                Intent Ukraine = new Intent(v.getContext(), UkraineTeamInfo.class);
                startActivity(Ukraine);
                break;
            case R.id.StatLink:
                Intent Link = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.uefa.com/womensunder19/season=2017/statistics/index.html"));
                startActivity(Link);
                break;
            case R.id.AddidasImage:
                Intent addidas = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.adidas.co.uk/"));
                startActivity(addidas);
                break;
            case R.id.EuroSportImage:
                Intent euro = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.eurosport.co.uk/"));
                startActivity(euro);
                break;
            case R.id.NorthernIrelandCrestBottomBanner:
                Intent ni = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.irishfa.com/"));
                startActivity(ni);
                break;
            default:
                break;
        }
    }

    public void onBackPressed() {

    }

}
