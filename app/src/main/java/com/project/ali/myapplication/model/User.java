package com.project.ali.myapplication.model;

/**
 * Created by Alistair on 02-Apr-17.
 *  http://www.androidtutorialshub.com/android-login-and-register-with-sqlite-database-tutorial/
 */
public class User {
    private int id;
    private String name;
    private String email;
    private String password;
//    private String fName;
//    private String lName;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public String getfName(){
//       return name;
//    }
//
//    public void setlName(String name){
//        this.name=name;
//    }
//
//
//    public String getlName(){
//        return name;
//    }
//
//    public void setfName(String name){
//        this.name=name;
//    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
