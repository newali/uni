package com.project.ali.myapplication.pages;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.ali.myapplication.R;

/**
 * Created by Alistair
 */
public class FixturesFragment extends Fragment implements View.OnClickListener {
    TextView knockout;
    ImageView Link;
    View myView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //instansiate the view
        myView = inflater.inflate(R.layout.fixtures_results_screen, container, false);

        //Call method knockout
        knockout();

        //Call NI
        NIClick();

        //Call eurosport
        EurosportClick();

        //Call Addidas
        AddidasClick();

        return myView;
    }

    /**
     * Setting the listener on the knockoutLink
     */
    public void knockout() {
            knockout = (TextView) myView.findViewById(R.id.KnockoutLink);
            knockout.setOnClickListener(this);
    }

    /**
     * Method to take the user to Northern Ireland page
     */
    public void NIClick() {

        Link = (ImageView) myView.findViewById(R.id.NorthernIrelandCrestBottomBanner);
        Link.setOnClickListener(this);

    }

    /**
     * Method to take the user to Addidas page
     */
    public void AddidasClick() {

        Link = (ImageView) myView.findViewById(R.id.AddidasImage);
        Link.setOnClickListener(this);

    }

    /**
     * Method to take the user to Eurosport page
     */
    public void EurosportClick() {

        Link = (ImageView) myView.findViewById(R.id.EuroSportImage);
        Link.setOnClickListener(this);

    }

    /**
     * Switch for user clicking the knockout link
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.KnockoutLink:
                Intent link = new Intent(v.getContext(), Knockout.class);
                startActivity(link);
                break;
            case R.id.AddidasImage:
                Intent addidas = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.adidas.co.uk/"));
                startActivity(addidas);
                break;
            case R.id.EuroSportImage:
                Intent euro = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.eurosport.co.uk/"));
                startActivity(euro);
                break;
            case R.id.NorthernIrelandCrestBottomBanner:
                Intent ni = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.irishfa.com/"));
                startActivity(ni);
                break;
            default:
                break;
        }
    }

    /**
     * On back pressed method does not work on fragment pages
     */
    public void onBackPressed() {

    }
}
