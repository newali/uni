package com.project.ali.myapplication.pages;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.LikeView;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;
import com.project.ali.myapplication.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Alistair
 * NOW used for Facebook, I have not renamed the class but it is for Facebook check in etc
 */
public class Gallery extends AppCompatActivity {
    ImageView imageView;
    TextView txtName, txtURL, txtGender, txtBd;
    Button btnShare;

    private ShareDialog shareDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallerys);

        shareDialog = new ShareDialog(this);
        //Setting the id with the appropriate views
        imageView = (ImageView) findViewById(R.id.imgPhoto);
        txtName = (TextView) findViewById(R.id.txtName);
       // txtURL = (TextView) findViewById(R.id.txtURL);
        txtGender = (TextView) findViewById(R.id.txtGender);
        txtBd = (TextView) findViewById(R.id.BirthdayTxt);

        //Way to share content
        btnShare = (Button) findViewById(R.id.btnShare);
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ShareDialog.canShow(ShareLinkContent.class)) {
                    //Setting users post to have details and image
                    ShareLinkContent linkContent = new ShareLinkContent.Builder()
                            .setContentTitle("U19 Womens Euros 2017")
                            .setContentDescription(
                                    "#BestOfNI")
                            .setContentUrl(Uri.parse("https://www.facebook.com/UEFAWU192017/?pnref=story"))
                           .setImageUrl(Uri.parse("http://www.irishfa.com/media/6268/uefamascot.jpg"))
                            //Cannot set users location automatically for check in so set to National Stadium
                            .setPlaceId("456552041199508")
                            .build();

                    shareDialog.show(linkContent);
                }
            }
        });

        //Call User Info method
        GetUserInfo();

        //Like
        LikeView likeView = (LikeView) findViewById(R.id.likeView);
        likeView.setLikeViewStyle(LikeView.Style.STANDARD);
        likeView.setAuxiliaryViewPosition(LikeView.AuxiliaryViewPosition.INLINE);
        //User can select the like button and like the u19 facebook page
        likeView.setObjectIdAndType(
                "https://www.facebook.com/UEFAWU192017/?pnref=story",
                LikeView.ObjectType.OPEN_GRAPH);


        //Share Dialog
        ShareButton fbShareButton = (ShareButton) findViewById(R.id.fb_share_button);
        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentTitle("U19 Womens Euros 2017")
                .setContentDescription(
                        "#BestOfNI")
                .setContentUrl(Uri.parse("https://www.facebook.com/UEFAWU192017/?pnref=story"))
                .setImageUrl(Uri.parse("http://www.irishfa.com/media/6268/uefamascot.jpg"))

                .build();
        fbShareButton.setShareContent(content);
    }

    /**
     * Method to get users info from facebook,
     * such as gender
     * birthdday and name
     */
    private void GetUserInfo() {
        //Code to obtain information from facebook
        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        // Application code
                        try {
                            //Returns users gender, birthday and name
                            String gender = object.getString("gender");
                            String birthday = object.getString("birthday");
                            String name = object.getString("name");
                            //   String id = object.getString("id");

                            txtName.setText(name);
                            //  txtURL.setText(id);
                            txtGender.setText(gender);
                            txtBd.setText(birthday);
                            if (object.has("picture")) {
                                String profilePicUrl = object.getJSONObject("picture").getJSONObject("data").getString("url");
                                Picasso.with(Gallery.this).load(profilePicUrl).into(imageView);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", //"id" +
                "gender,name,birthday,picture.type(large)");
        request.setParameters(parameters);
        request.executeAsync();

    }




}