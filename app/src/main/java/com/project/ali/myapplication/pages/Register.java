package com.project.ali.myapplication.pages;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.project.ali.myapplication.R;
//import com.project.ali.myapplication.app.AppConfig;
//import com.project.ali.myapplication.app.AppController;
//import com.project.ali.myapplication.backEnd.Setup;
//import com.project.ali.myapplication.helper.SQLiteHandler;
//import com.project.ali.myapplication.helper.SessionManager;
//import com.project.ali.myapplication.middleTier.User;


/**
 * Created by Alistair
 * Original code and some code taken from tutorial to connect to db
 *  * This page is now redundant as the login activity page is used for the app and SQLite
 * HOWEVER the reader can view my various attempts at different connections and the original code
 */
public class Register extends Welcome {
  //  private static final String TAG = com.project.ali.myapplication.pages.Login.class.getSimpleName();

    protected EditText username;
    protected EditText password;
    protected EditText fName;
    protected EditText lName;
    protected EditText email;
    protected CheckBox male;
    protected CheckBox female;
    protected CheckBox yes;
    protected CheckBox no;
    private Button registerBtn;
   // private ProgressDialog pDialog;
   // private SessionManager session;
   // private SQLiteHandler db;
   // private EditText name;

 //   User selectedUser;

    /**
    *Main method for Register
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //testing allow network access
        //StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
       // StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.register_screen);

        // finding database
        // declaring database from dataAccess
        //   Setup bjdb = new Setup(this);
//       eurodb = new Setup(this);

        // finding edit texts
        fName = (EditText) findViewById(R.id.firstNameEditText);
        lName = (EditText) findViewById(R.id.lastnameEditText);
        username = (EditText) findViewById(R.id.usernameEditText);
        password = (EditText) findViewById(R.id.passwordEditText);
        email = (EditText) findViewById(R.id.emailEditText);
        male = (CheckBox) findViewById(R.id.MaleCheckBox);
        female = (CheckBox) findViewById(R.id.FemaleCheckBox);
        yes = (CheckBox) findViewById(R.id.YesCheckBox);
        no = (CheckBox) findViewById(R.id.NoCheckBox);
        male = (CheckBox) findViewById(R.id.MaleCheckBox);
        female = (CheckBox) findViewById(R.id.FemaleCheckBox);
        yes = (CheckBox) findViewById(R.id.YesCheckBox);
        no = (CheckBox) findViewById(R.id.NoCheckBox);


        // finding buttons
        registerBtn = (Button) findViewById(R.id.RegisterButton);



//        eurodb = new Setup(getApplicationContext());


     //   addUser();

       // viewAll();

        Back();


        Reg();

        TermsLink();
    }

//    public void addUser(){
//
//        registerBtn.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//
//                eurodb.createUser(new User(fName.getText().toString(), lName.getText().toString(),
//                        email.getText().toString(), username.getText().toString(), password.getText().toString()));
//            //    sex.getText().toString(), firstTime.getText().toString()
//
//            }
//        });
//    }
//
//    public void viewAll() {
//        Cursor result = eurodb.getAllData();
//        if (result.getCount()==0){
//
//            showMessage("Error","No data found");
//            return;
//        }
//        StringBuffer buffer = new StringBuffer();
//        while (result.moveToNext()){
//            buffer.append("First name : "+result.getString(0)+"\n");
//            buffer.append("Last name : "+result.getString(1)+"\n");
//            buffer.append("Email : "+result.getString(2)+"\n");
//            buffer.append("Username : "+result.getString(3)+"\n");
//            buffer.append("Password : "+result.getString(4)+"\n");
//            buffer.append("Sex : "+result.getString(5)+"\n");
//            buffer.append("FirstTime : "+result.getString(6)+"\n");
//        }
//
//        showMessage("Data", buffer.toString());
//    }
//
//    public void delete(View v){
//        Toast.makeText(getApplicationContext(),"Deleted user account",Toast.LENGTH_LONG).show();
//
//        eurodb.deleteUser(selectedUser);
//        finish();
//    }

    //Back button to take the user back to the Welcome page when clicked.
    public void backButton(View v) {

        startActivity(new Intent(Register.this, Welcome.class));
    }

    public void showMessage(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }



//        //call method sex
//        Sex();
//
//        //call method first time in NI
//        FirstTimeInNI();
//
      //Call method for terms and Conditions
   //     TermsLink();
//
        //Call method for back button
      //  Back();
//
//        //Call method for Register button
//        Reg();
//
//        //Call set ids
//        idSet();
//
//        isNetworkAvailable();
//
//        // Progress dialog
//        pDialog = new ProgressDialog(this);
//        pDialog.setCancelable(false);
//
//        // Session manager
//        session = new SessionManager(getApplicationContext());
//
//        // SQLite database handler
//        db = new SQLiteHandler(getApplicationContext());
//
//        // Check if user is already logged in or not
//        if (session.isLoggedIn()) {
//            // User is already logged in. Take him to main activity
//            Intent intent = new Intent(Register.this,
//                    MainActivity.class);
//            startActivity(intent);
//            finish();
//
//        }
//
//
//    }
//
//    //Checking network connection
//    public boolean isNetworkAvailable(){
//        ConnectivityManager cm = (ConnectivityManager)
//                getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
//        //If no network is available networkinfo will be null
//        if (networkInfo != null && networkInfo.isConnected()) {
//
//            return true;
//        }
//        return false;
//    }
//
//
//
//    /**
//     * idSet method to set ids
//     */
//    public void idSet() {
//        fName = (EditText) findViewById(R.id.firstNameEditText);
//        lName = (EditText) findViewById(R.id.lastnameEditText);
//        username = (EditText) findViewById(R.id.usernameEditText);
//        password = (EditText) findViewById(R.id.passwordEditText);
//        email = (EditText) findViewById(R.id.emailEditText);
//        male = (CheckBox) findViewById(R.id.MaleCheckBox);
//        female = (CheckBox) findViewById(R.id.FemaleCheckBox);
//        yes = (CheckBox) findViewById(R.id.YesCheckBox);
//        no = (CheckBox) findViewById(R.id.NoCheckBox);
//        registerBtn = (Button) findViewById(R.id.RegisterButton);
//    }

    /**
     * registerClick method to check if details have been entered
     */
//    public void registerClick(){
//        // Register Button Click event
//        registerBtn.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View view) {
//                String name = fName.getText().toString().trim();
//                String lname = lName.getText().toString().trim();
//                String Username = username.getText().toString().trim();
//                String pass = password.getText().toString().trim();
//
//
//                if (!name.isEmpty() && !lname.isEmpty() && !Username.isEmpty() &&!pass.isEmpty()) {
//                    registerUser(name, lname, pass);
//                } else {
//                    Toast.makeText(getApplicationContext(),
//                            "Please enter your details!", Toast.LENGTH_LONG)
//                            .show();
//                }
//            }
//        });
//    }

    /*
    *Method for sex
     */
    private void Sex() {
        //Call method male
        Male();
        //Call method female
        Female();

    }



    /*
    *Method for Male
     */
    private void Male() {
        //Checkbox for male
        final CheckBox MaleCheckbox = (CheckBox) findViewById(R.id.MaleCheckBox);
        //Checkbox for Female
        final CheckBox FemaleCheckBox = (CheckBox) findViewById(R.id.FemaleCheckBox);

        MaleCheckbox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Allow only one to be checked
                if (MaleCheckbox.isChecked()) {
                    FemaleCheckBox.setChecked(false);
                }
            }
        });
    }

    /*
    *Method for Female
     */
    private void Female() {
        //Checkbox for male
        final CheckBox MaleCheckbox = (CheckBox) findViewById(R.id.MaleCheckBox);
        //Checkbox for Female
        final CheckBox FemaleCheckBox = (CheckBox) findViewById(R.id.FemaleCheckBox);

        FemaleCheckBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Allow only one to be checked
                if (FemaleCheckBox.isChecked()) {
                    MaleCheckbox.setChecked(false);
                }
            }
        });
    }


    /*
    *Method for First time in NI
     */
    private void FirstTimeInNI() {
        //Call method yes
       Yes();
        //Call method No
        No();
    }

    /*
    *   Method for Yes
     */
    private void Yes() {
        //Checkbox for Yes
        final CheckBox YesCheckbox = (CheckBox) findViewById(R.id.YesCheckBox);
        //Checkbox for No
        final CheckBox NoCheckBox = (CheckBox) findViewById(R.id.NoCheckBox);

        YesCheckbox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Allow only one to be checked
                if (YesCheckbox.isChecked()) {
                    NoCheckBox.setChecked(false);
                }
            }
        });
    }
    /*
    * Method for No
     */
    private void No() {
        //Checkbox for Yes
        final CheckBox YesCheckbox = (CheckBox) findViewById(R.id.YesCheckBox);
        //Checkbox for No
        final CheckBox NoCheckBox = (CheckBox) findViewById(R.id.NoCheckBox);

        NoCheckBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Allow only one to be checked
                if (NoCheckBox.isChecked()) {
                    YesCheckbox.setChecked(false);
                }
            }
        });
    }

//    public void Terms(){
//
//        TextView TermsText = (TextView) findViewById(R.id.termsAndConditionsText);
//        TermsText.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(v.getContext(), Terms.class);
//                startActivity(intent);
//            }
//        });
//    }

    /**
     * Back button to take user to the welcome page
     */
    public void Back(){
        Button BackBtn = (Button) findViewById(R.id.BackButtonRegister);
        BackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), Welcome.class);
                startActivity(i);
            }
        });
    }


    /**
     * Calling the edittexts and button
     * Setting requirements on the edit text and toasts to appear if no information is presented
     * No need to validate email using normal verification
     * it will not catch common typos.
     * It does not prevent people from entering invalid or made-up email addresses, or entering someone else's address.
     * The best action is to send a verfication email which would be done in further development of the App. Or else
     * leave it as an optional field
     * Sex and first time are not mandatory fields so don't require validation.
     */
    public void Reg(){
            username = (EditText) findViewById(R.id.usernameEditText);
            password = (EditText) findViewById(R.id.passwordEditText);
            registerBtn = (Button) findViewById(R.id.RegisterButton);
            fName=(EditText) findViewById(R.id.firstNameEditText);
            lName=(EditText) findViewById(R.id.lastnameEditText);
            email=(EditText) findViewById(R.id.emailEditText);


        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fName.getText().toString().equals("")){
                    Toast.makeText(Register.this, "First name cannot be empty", Toast.LENGTH_LONG).show();
                    return;
                }
                if (lName.getText().toString().equals("")){
                    Toast.makeText(Register.this, "Last name cannot be empty", Toast.LENGTH_LONG).show();
                    return;
                }
//                if (!male.isChecked() && !female.isChecked()) {
//                    Toast.makeText(Register.this, "Please select Sex", Toast.LENGTH_LONG).show();
//                    return;
//                }
                if (email.getText().toString().equals("")) {
                    Toast.makeText(Register.this, "Email cannot be empty", Toast.LENGTH_LONG).show();
                    return;
                }
                if (username.getText().toString().equals("")) {
                    Toast.makeText(Register.this, "Username cannot be empty", Toast.LENGTH_LONG).show();
                    return;
                }
                if (password.getText().toString().equals("")) {
                    Toast.makeText(Register.this, "Password cannot be empty", Toast.LENGTH_LONG).show();
                    return;
                }
                if (username.length() > 25 ) {
                    Toast.makeText(Register.this, "Username length must be less than 25 characters", Toast.LENGTH_LONG).show();
                    return;
                }
                if (password.length() > 25 ) {
                    Toast.makeText(Register.this, "Username length must be less than 25 characters", Toast.LENGTH_LONG).show();
                    return;
                }
//                if (yes.isChecked()==false && no.isChecked()==false) {
//                    Toast.makeText(Register.this, "Please select first time in NI", Toast.LENGTH_LONG).show();
//                    return;
//                }


                Intent intent = new Intent(v.getContext(), Login.class);
                startActivity(intent);


            }
        });
    }


    /**
     * When clicked image of terms and conditions will load up.
     */
    public void TermsLink() {
        final TextView terms = (TextView) findViewById(R.id.termsAndConditionsText);
        final TextView text = (TextView) findViewById(R.id.termsTextInfo);
        final Button termsX = (Button)findViewById(R.id.termsButton);

        termsX.setVisibility(View.INVISIBLE);
        text.setVisibility(View.INVISIBLE);

        terms.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                termsX.setVisibility(View.VISIBLE);
                text.setVisibility(View.VISIBLE);

                //Calling the terms method
                TermsImage();

            }
        });
    }

    /**
     *  Method so that clicking the x on the image removes the text box
     */
    public void TermsImage() {

        final Button termsX = (Button) findViewById(R.id.termsButton);
        final TextView text = (TextView) findViewById(R.id.termsTextInfo);

        termsX.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                v.equals(termsX);
                termsX.setVisibility(View.INVISIBLE);
                text.setVisibility(View.INVISIBLE);
            }
        });
    }
}
//
//    /**
//     * Function to store user in MySQL database will post params(tag, name,
//     * email, password) to register url
//     * */
//    private void registerUser(final String name, final String email,
//                              final String password) {
//        // Tag used to cancel the request
//        String tag_string_req = "req_register";
//
//        pDialog.setMessage("Registering ...");
//        showDialog();
//
//        StringRequest strReq = new StringRequest(Request.Method.POST,
//                AppConfig.URL_REGISTER, new Response.Listener<String>() {
//
//            @Override
//            public void onResponse(String response) {
//                Log.d(TAG, "Register Response: " + response.toString());
//                hideDialog();
//
//                try {
//                    JSONObject jObj = new JSONObject(response);
//                    boolean error = jObj.getBoolean("error");
//                    if (!error) {
//                        // User successfully stored in MySQL
//                        // Now store the user in sqlite
////                        String uid = jObj.getString("uid");
//
//                      //  JSONObject user = jObj.getJSONObject("user");
//                        JSONObject user = jObj.getJSONObject("User");
//                        String fName = user.getString("FirstName");
//                        String lName = user.getString("Lastname");
//                        String Username = user.getString("Username");
//                        String Password = user.getString("Password");
//
//
//
//
//                        // Inserting row in users table
//                        db.addUser(fName,lName, Username,Password);
//
//                        Toast.makeText(getApplicationContext(), "User successfully registered. Try login now!", Toast.LENGTH_LONG).show();
//
//                        // Launch login activity
//                        Intent intent = new Intent(
//                                Register.this,
//                                Login.class);
//                        startActivity(intent);
//                        finish();
//                    } else {
//
//                        // Error occurred in registration. Get the error
//                        // message
//                        String errorMsg = jObj.getString("error_msg");
//                        Toast.makeText(getApplicationContext(),
//                                errorMsg, Toast.LENGTH_LONG).show();
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "Registration Error: " + error.getMessage());
//                Toast.makeText(getApplicationContext(),
//                        error.getMessage(), Toast.LENGTH_LONG).show();
//                hideDialog();
//            }
//        }) {
//
//            @Override
//            protected Map<String, String> getParams() {
//                // Posting params to register url
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("name", name);
//                params.put("email", email);
//                params.put("password", password);
//
//                return params;
//            }
//
//        };
//
//        // Adding request to request queue
//        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
//    }
//
//
//
//    private void showDialog() {
//        if (!pDialog.isShowing())
//            pDialog.show();
//    }
//
//    private void hideDialog() {
//        if (pDialog.isShowing())
//            pDialog.dismiss();
//    }
//}



