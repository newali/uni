package com.project.ali.myapplication.pages;


import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.project.ali.myapplication.R;

import java.io.IOException;
import java.util.List;

/**
 * Created by Alistair
 */
public class NationalStadiumDirections extends AppCompatActivity {

    View myView;
    LocationRequest mLocationRequest;


    //Create GoogleMap object
    private GoogleMap googleMap;
    private static final LatLng national = new LatLng(54.582625, -5.955189);
//    private static final LatLng mourneview = new LatLng(54.453889,-6.336389);
//    private static final LatLng Shamrock = new LatLng( 54.413056,-6.457778);
//    private static final LatLng Showgrounds = new LatLng(54.87010,-6.26507);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Setting the screen as the login screen
        setContentView(R.layout.national_stadium_directions);


        //Call method for GPS
        Gps();

        //Call method for processMap
        processMap();

        //Call onConnected method
        onConnected();
    }


    /**
     * Process Map using fragment
     */
    public void processMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

        }
        if (googleMap != null) {
            setUpMap();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        processMap();
    }

    /**
     * On click method for xml button to search
     * @param view
     */
    public void onSearch( View view) {
        //Find the edit text
        EditText Location = (EditText) findViewById(R.id.TFAddress);
        String location = Location.getText().toString();

        List<Address> addressList = null;



        LatLng latLng = null;
        if (location != null || !location.equals("")) {
            //Allows user to search any string in form of long or lat
            Geocoder geocoder = new Geocoder(this);
            try {
                //List of objects from geocoder and only one result
                addressList = geocoder.getFromLocationName(location, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }



            Address address = addressList.get(0);
            latLng = new LatLng(address.getLatitude(), address.getLongitude());
            googleMap.addMarker(new MarkerOptions().position(latLng).title("Marker"));
            googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

        }else{
            Toast.makeText(getApplicationContext(),
                    "No search Information entered", Toast.LENGTH_LONG)
                    .show();
        }


    }

    /**
     * Setting up the markers
     */
    public void setUpMap() {
        googleMap.addMarker(new MarkerOptions().position(national)
                .title("National Stadium")
                .snippet("Capacity: 15,000")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.stadiums)));
        //now zoom camera
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(national, 10));

        googleMap.setMyLocationEnabled(true);
    }



    /**
     * Method for zooming on xml on click
     * @param view
     */
    public void onZoom(View view){
        //check which button the user clicks, out or in
        if(view.getId()== R.id.BZoomIn){
            //if zoom in button is clicked then camera zoom in
        googleMap.animateCamera(CameraUpdateFactory.zoomIn());
        }
        if (view.getId()== R.id.BZoomOut){
            //if zoom out button is clicked then camera zoom out
            googleMap.animateCamera(CameraUpdateFactory.zoomOut());
        }

    }



    /**
     * On click method for change map type in xml class
     */
    public void changeType(View view){
        //check if map type is normal
        if (googleMap.getMapType()== GoogleMap.MAP_TYPE_NORMAL){
            //set to hybrid
            googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        }else {
            //map type normal
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }

    }


    /**
     * Method to take user to directions page
     */
    public void Gps(){
            Button GPS = (Button) findViewById(R.id.GPSDirectionsNational);
            GPS.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent GPSIntent = new Intent(NationalStadiumDirections.this, NationalGps.class);
                    startActivity(GPSIntent);
                }
            });
    }

    public void onConnected() {
        mLocationRequest = LocationRequest.create();
        //High accuracy to determine users location. Will use a lot of battery
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        //Set length of time between updating users location
        //1000 milliseconds
        mLocationRequest.setInterval(1000);


    }

}
