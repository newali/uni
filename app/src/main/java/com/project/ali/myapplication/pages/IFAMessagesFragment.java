package com.project.ali.myapplication.pages;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.project.ali.myapplication.R;



/**
 * Created by Alistair
 */
public class IFAMessagesFragment extends Fragment implements View.OnClickListener {
    Button Click;
    ImageView Link;
    View myView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //instansiate the view
        myView = inflater.inflate(R.layout.ifa_messages, container, false);

        //Call TwitterProgress
        TwitterProgress();

        //Call NI
        NIClick();

        //Call eurosport
        EurosportClick();

        //Call Addidas
        AddidasClick();


        return myView;
    }

    /**
     * Twitter progress method
     */
    public void TwitterProgress() {
        Click = (Button) myView.findViewById(R.id.TwitterProgress);
        Click.setOnClickListener(this);
    }

    /**
     * Method to take the user to Northern Ireland page
     */
    public void NIClick() {

        Link = (ImageView) myView.findViewById(R.id.NorthernIrelandCrestBottomBanner);
        Link.setOnClickListener(this);

    }

    /**
     * Method to take the user to Addidas page
     */
    public void AddidasClick() {

        Link = (ImageView) myView.findViewById(R.id.AddidasImage);
        Link.setOnClickListener(this);

    }

    /**
     * Method to take the user to Eurosport page
     */
    public void EurosportClick() {

        Link = (ImageView) myView.findViewById(R.id.EuroSportImage);
        Link.setOnClickListener(this);

    }




    /**
     * Switch for user clicking a button
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.TwitterProgress:
                Intent Tweet = new Intent(v.getContext(), Twitters.class);
                startActivity(Tweet);
                break;
            case R.id.AddidasImage:
                Intent addidas = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.adidas.co.uk/"));
                startActivity(addidas);
                break;
            case R.id.EuroSportImage:
                Intent euro = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.eurosport.co.uk/"));
                startActivity(euro);
                break;
            case R.id.NorthernIrelandCrestBottomBanner:
                Intent ni = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.irishfa.com/"));
                startActivity(ni);
                break;
            default:
                break;
        }
    }


}
