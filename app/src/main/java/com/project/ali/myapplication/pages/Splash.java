package com.project.ali.myapplication.pages;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.project.ali.myapplication.R;

/**
 * Created by Alistair
 */
public class Splash extends Activity{

    //create media player
    MediaPlayer mySound;

    //Release the sound whenever the page changes. i.e whenever the splash finishes so will the
    //sound.
    @Override
    protected void onPause(){
        super.onPause();
        mySound.release();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //link to loading screen
        setContentView(R.layout.loading_screen);
        //creating the file for inside media player
        mySound = MediaPlayer.create(this, R.raw.crowd_cheer);
        mySound.start();

        mySound.setVolume(100,100);

//link to splashImage set up on the loading screen and call the image view
        final ImageView iv = (ImageView) findViewById(R.id.SplashImageView);
        //link to the created rotate xml class
        final Animation an = AnimationUtils.loadAnimation(getBaseContext(), R.anim.rotate);
        //link to the fade out built in method
        final Animation an2 = AnimationUtils.loadAnimation(getBaseContext(), R.anim.abc_fade_out);
        //the image view calls the first animation
        iv.startAnimation(an);
        an.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            /**
             * Method to call animation and open the next class, Welcome
             */
            public void onAnimationEnd(Animation animation) {
                //calling the second animation
                iv.startAnimation(an2);
                //finish method
                finish();
                //Intent i = new Intent(getBaseContext(), MainActivity.class);
                //startActivity(i);
                Intent i = new Intent(getBaseContext(), Welcome.class);
                startActivity(i);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


    }





}
