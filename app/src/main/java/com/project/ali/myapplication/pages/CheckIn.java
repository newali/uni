package com.project.ali.myapplication.pages;

/**
 * Created by Alistair
 */

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;


import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import android.content.pm.Signature;
import android.view.View;
import android.widget.Button;

import com.project.ali.myapplication.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
//import java.security.Signature;
import java.util.Arrays;
import java.util.List;

/**
 * Check in main method
 */
public class CheckIn extends AppCompatActivity {

    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private AccessToken accessToken ;
    private final static String TAG = CheckIn.class.getName().toString();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Initialize facebook
        FacebookSdk.sdkInitialize(getApplicationContext(), new FacebookSdk.InitializeCallback() {
            @Override
            public void onInitialized() {
                //Get the current accesstoken
                accessToken = AccessToken.getCurrentAccessToken();
                if (accessToken == null) {
                    Log.d(TAG, "Not logged in yet");
                } else {
                    Log.d(TAG, "Logged in");
                    Intent main = new Intent(CheckIn.this,Gallery.class);
                    startActivity(main);

                }
            }
        });

        //Setting the page as check in
        setContentView(R.layout.check_in);


        //Call the facebook method
        facebook();

        //callback manager for login result
                callbackManager = CallbackManager.Factory.create();

        //access token will check if user has already checked in
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {
                accessToken = newToken;
            }
        };

        //setting the login button to login_button
        LoginButton loginButton = (LoginButton)findViewById(R.id.login_button);

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                accessToken = loginResult.getAccessToken();

                //This intent will then take the user through to the gallery page
                Intent main = new Intent(CheckIn.this,Gallery.class);
                startActivity(main);

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException exception) {

            }
        });


        List<String> permissionNeeds = Arrays.asList("user_friends","email","user_birthday");
        loginButton.setReadPermissions(permissionNeeds);

        accessTokenTracker.startTracking();

        //Hashkey
        showHashKey(this);
    }


    /**
     * Method for hashkey
     * @param context
     */
    public static void showHashKey(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    "com.project.ali.myapplication", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest messageDigest = MessageDigest.getInstance("SHA");
                messageDigest.update(signature.toByteArray());
                Log.i("KeyHash:", Base64.encodeToString(messageDigest.digest(), Base64.DEFAULT));

            }
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NoSuchAlgorithmException e) {
        }
    }


    /**
     *
     * @param requestCode
     * @param responseCode
     * @param intent
     */
    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        super.onActivityResult(requestCode, responseCode, intent);
        //Facebook login
        callbackManager.onActivityResult(requestCode, responseCode, intent);

    }

    /**
     * onPause method
     */
    @Override
    protected void onPause() {

        super.onPause();
    }

    /**
     * onStop method
     */
    protected void onStop() {
        super.onStop();
        accessTokenTracker.stopTracking();
    }


    public void facebook(){
        //Back button for once the user is logged in
        Button BackBtn = (Button) findViewById(R.id.RTFButton);

        BackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), Gallery.class);
                startActivity(i);
            }
        });
    }
    }



