package com.project.ali.myapplication.pages;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.project.ali.myapplication.R;

/**
 * Created by Alistair
 * Class needs developed in the future as it does very little at the minute
 */
public class ForgotPassword extends MainActivity{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset_password);

        //BackBtnClick
        BackBtnClick();
    }

    /**
     * Method for back button to take user to login
     */
    public void BackBtnClick(){
        //Back button and setting a click listener
        Button BackBtn = (Button) findViewById(R.id.BackButtonForgotPassword);
        BackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), Login.class);
                startActivity(i);
            }
        });
    }
}

