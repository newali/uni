//package com.project.ali.myapplication.helper;
//
//import android.content.Context;
//import android.content.SharedPreferences;
//import android.util.Log;
//
///**
// * Taken from online source
// * Ravi Tamada
// * Android login and registration with php, mysql and sqlite
// * www.androidhive.info/2012/01/android-login-and-registration-with-php-mysql-and-sqlite/
// */
//public class SessionManager {
//    //logcat tag
//    private static String TAG = SessionManager.class.getSimpleName();
//
//    //Shared preferences
//    SharedPreferences pref;
//    SharedPreferences.Editor editor;
//    Context _context;
//
//    //shared pref mode
//    int PRIVATE_MODE = 0;
//
//    //Shared preferences file name
//    private static final String PREF_NAME = "Login";
//    private static final String KEY_IS_LOGGED_IN = "Is logged in";
//
//    public SessionManager(Context context) {
//        this._context = context;
//        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
//        editor = pref.edit();
//    }
//
//    public void setLogin(boolean isLoggedIn) {
//        editor.putBoolean(KEY_IS_LOGGED_IN, isLoggedIn);
//
//        //commit changes
//        editor.commit();
//
//        Log.d(TAG, "user login session modified");
//
//    }
//
//    public boolean isLoggedIn() {
//        return pref.getBoolean(KEY_IS_LOGGED_IN, false);
//
//    }
//}
