package com.project.ali.myapplication.pages;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.project.ali.myapplication.R;

import net.hockeyapp.android.FeedbackManager;

/**
 * Created by Alistair
 */
public class BugReport extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bug_report);

        //call bug button method
        bugButton();

        //Call feedback built in method
        FeedbackManager.register(this);
    }

    /**
     * Click to submit bug report button
     * Linked to feedback manager imported from HockeyApp
     */
    public void bugButton() {
        Button bugReport = (Button) findViewById(R.id.bugReport);
        bugReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FeedbackManager.showFeedbackActivity(BugReport.this);
            }
        });


    }
}

