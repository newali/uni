package com.project.ali.myapplication.pages;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.project.ali.myapplication.R;

/**
 * Created by Alistair
 */
public class UkraineTeamInfo extends AppCompatActivity {
    ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ukraine_team_information);


        //Finding the list from the xml page
        listView = (ListView) findViewById(R.id.listUkraine);

        //Array to hold values which will be displayed on the xml page
        String[] values = new String[]{
                "Goalkeeper: A.Herasymchuk",
                "Goalkeeper: D.Bondarchuk",
                "Defender: A.Komanda",
                "Defender: K.Tataryn",
                "Midfielder: O.Levchyshyna",
                "Midfielder: I.Rybalkina",
                "Midfielder: Y.Shevchuk",
                "Midfielder: K.Kozub",
                "Midfielder: A.Kumeda",
                "Midfielder: T.Polyukhovych",
                "Midfielder: T.Bubnyak",
                "Midfielder: N.Cherepania",
                "Midfielder: Y.Derkach",
                "Forwards: N.Hryb",
                "Forwards: V.Budaieva",
                "Forwards: O.Manziuk",
                "Forwards: D.Kovtun",
                "Forwards: S.Ruban"

        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);


        // Listview
        listView.setAdapter(adapter);

        //OnItemClickListener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition = position;

                //Get the value of the item clicked
                String itemValue = (String) listView.getItemAtPosition(position);

                //Toast to show information clicked.
                //No real purpose but to highlight info
                Toast.makeText(getApplicationContext(),
                        itemValue, Toast.LENGTH_SHORT)
                        .show();


            }


        });
    }
}

