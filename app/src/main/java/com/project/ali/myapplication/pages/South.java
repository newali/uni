package com.project.ali.myapplication.pages;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.project.ali.myapplication.R;

/**
 * Created by Alistair
 */
public class South extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.south1);

        //Calling bangor method
        bangor();

        //callimg lurgan
        lurgan();

        //calling downpatrick
        downpatrick();

        //Call the eurosport method
        eurosportClick();

        //Call the adidas method
        adidasClick();

        //call the ni method
        Northern();
    }


    /**
     * Method to take the user to Eurosport
     */
    public void eurosportClick() {

        ImageView reviews = (ImageView) findViewById(R.id.EuroSportImage);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent eurosport = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.eurosport.co.uk/"));
                startActivity(eurosport);
            }
        });
    }


    /**
     * Method to take user to Northern Ireland webpage
     */
    public void Northern() {
        ImageView reviews = (ImageView) findViewById(R.id.NorthernIrelandCrestBottomBanner);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent NI = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.irishfa.com/"));
                startActivity(NI);
            }
        });
    }

    /**
     * Method to take the user to Adidas
     */
    public void adidasClick() {

        ImageView reviews = (ImageView) findViewById(R.id.AddidasImage);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent adidas = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.adidas.co.uk/"));
                startActivity(adidas);
            }
        });
    }


    /**
     * Method to handle click of taking user to bangor
     */
    public void bangor() {
        ImageView reviews = (ImageView) findViewById(R.id.bangorLadiesSouthOne);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //calling the method to open the home page
                Intent arm = new Intent(South.this, bangor.class);
                startActivity(arm);
            }
        });
    }


    /**
     * Method to handle click of taking user to lurgan
     */
    public void lurgan() {
        ImageView reviews = (ImageView) findViewById(R.id.lurganladiesChamp);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //calling the method to open the home page
                Intent lur = new Intent(South.this, Lurgan.class);
                startActivity(lur);
            }
        });
    }

    /**
     * Method to handle click of taking user to downpatrick
     */
    public void downpatrick() {
        ImageView reviews = (ImageView) findViewById(R.id.downpatrickLadiesChamp);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //calling the method to open the home page
                Intent down = new Intent(South.this, Downpatrick.class);
                startActivity(down);
            }
        });
    }
}

