package com.project.ali.myapplication.pages;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;


import com.project.ali.myapplication.R;

/**
 * Created by Alistair
 */
public class ResetPassword extends AppCompatActivity {

    //Main method for Rest password/forgot password
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Setting the screen as the reset password screen
        setContentView(R.layout.reset_password);

        //Call the eurosport method
        eurosportClick();

        //Call the adidas method
        adidasClick();

        //back button method being called
        back();

        //Call the Northern Ireland clicked method
        Northern();

    }

    /**
     * Method to take the user to Eurosport
     */
    public void eurosportClick() {

        ImageView reviews = (ImageView) findViewById(R.id.EuroSportImage);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent eurosport = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.eurosport.co.uk/"));
                startActivity(eurosport);
            }
        });
    }

    /**
     * Method to take the user to Adidas
     */
    public void adidasClick() {

        ImageView reviews = (ImageView) findViewById(R.id.AddidasImage);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent adidas = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.adidas.co.uk/"));
                startActivity(adidas);
            }
        });
    }

    /**
     * Method to take user to Northern Ireland webpage
     */
    public void Northern() {
        ImageView reviews = (ImageView) findViewById(R.id.NorthernIrelandCrestBottomBanner);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent NI = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.irishfa.com/"));
                startActivity(NI);
            }
        });
    }


    /**
     * Back button and setting a click listener
      */
    public void back(){
        Button BackBtn = (Button) findViewById(R.id.BackButtonForgotPassword);
        BackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), Login.class);
                startActivity(i);
            }
        });

}}