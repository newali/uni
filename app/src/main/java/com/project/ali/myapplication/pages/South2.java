package com.project.ali.myapplication.pages;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.project.ali.myapplication.R;

/**
 * Created by Alistair
 */
public class South2 extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.south2);

        //Call the craigavon method
        craigavon();

        //Call st james swifts method
        stJamesSwift();

        //Call banbridge method
        banbridge();

        //Call the eurosport method
        eurosportClick();

        //Call the adidas method
        adidasClick();

        //call the ni method
        Northern();
    }


    /**
     * Craigavon method
     */
    public void craigavon(){
        ImageView reviews = (ImageView) findViewById(R.id.craigavonladiesSouth2);
        reviews.setOnClickListener(new View.OnClickListener() {
        public void onClick(View v) {

            //calling the method to open the home page
            Intent arm = new Intent(South2.this, Craigavon.class);
            startActivity(arm);
        }
    });

    }


    /**
     * Method to take the user to Eurosport
     */
    public void eurosportClick() {

        ImageView reviews = (ImageView) findViewById(R.id.EuroSportImage);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent eurosport = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.eurosport.co.uk/"));
                startActivity(eurosport);
            }
        });
    }


    /**
     * Method to take user to Northern Ireland webpage
     */
    public void Northern() {
        ImageView reviews = (ImageView) findViewById(R.id.NorthernIrelandCrestBottomBanner);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent NI = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.irishfa.com/"));
                startActivity(NI);
            }
        });
    }

    /**
     * Method to take the user to Adidas
     */
    public void adidasClick() {

        ImageView reviews = (ImageView) findViewById(R.id.AddidasImage);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent adidas = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.adidas.co.uk/"));
                startActivity(adidas);
            }
        });
    }

    /**
     * Method to handle click of taking user to stJamesSwifts
     */
    public void stJamesSwift() {
        ImageView reviews = (ImageView) findViewById(R.id.stJamesSwiftladiesChamp);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //calling the method to open the home page
                Intent st = new Intent(South2.this, StJamesSwift.class);
                startActivity(st);
            }
        });
    }

    /**
     * Method to handle click of taking user to banbridge
     */
    public void banbridge() {
        ImageView reviews = (ImageView) findViewById(R.id.banbridgeLadiesChamp);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //calling the method to open the home page
                Intent col = new Intent(South2.this, Banbridge.class);
                startActivity(col);
            }
        });
    }

}

