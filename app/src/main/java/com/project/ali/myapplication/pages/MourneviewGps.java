package com.project.ali.myapplication.pages;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.project.ali.myapplication.R;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import module.DirectionFinder;
import module.DirectionFinderListener;
import module.Route;

/**
 * Created by Alistair
 * Original and code taken from tutorial
 */
public class MourneviewGps extends AppCompatActivity implements OnMapReadyCallback, DirectionFinderListener {

    //Strings for autocomplete form for origin and destination
    String str[]={"Ballymena Showgrounds", "Belfast National Stadium", "Lurgan Mourneview", "Portadown Shamrock Park",
            "Antrim town", "Lisburn", "Bangor Northern Ireland", "Carrickfergus", "Portrush", "Giants Causeway", "Armagh", "Newry", "Larne",
            "Titanic Belfast", "Londonderry", "Derry", "Coleraine","Omagh", "Cookstown", "Belfast",
            "Portadown", "Ballymena", "Lurgan","Bushmills Distillery",
            "Ulster American Folk Park"};
    private AutoCompleteTextView etOriginMourne;
    private AutoCompleteTextView etDestinationMourne;
    private List<Marker> Markers = new ArrayList<>();
    private static final LatLng mourneview = new LatLng(54.454392, -6.336736);
    LocationRequest mLocationRequest;
    private GoogleMap mMap;
    private Button btnFindPath;
    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mourneview_gps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        btnFindPath = (Button) findViewById(R.id.btnFindPath);

        //Setting the etOrigin to auto
        etOriginMourne = (AutoCompleteTextView)findViewById(R.id.etOriginMourne);

        //Array adapter to the dropdown list and connecting with the string array
        ArrayAdapter<String> adapt = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, str);

        //Bind the adapter to actv
        etOriginMourne.setAdapter(adapt);

        //Setting etDestination to auto
        etDestinationMourne = (AutoCompleteTextView) findViewById(R.id.etDestinationMourne);

        //dropdown item to hold destination information
        ArrayAdapter<String> adaptDest = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, str);
        etDestinationMourne.setAdapter(adaptDest);


        btnFindPath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRequest();
            }
        });

        //Call onConnected method
        onConnected();
    }



    /**
     * Method to display toasts if text boxes are empty
     * When find path is selected this method will run
     */
    private void sendRequest() {
        String origin = etOriginMourne.getText().toString();
        String destination = etDestinationMourne.getText().toString();
        //Origin empty
        if (origin.isEmpty()) {
            Toast.makeText(this, "Please enter origin address", Toast.LENGTH_SHORT).show();
            return;
        }
        //Desintation empty
        if (destination.isEmpty()) {
            Toast.makeText(this, "Please enter destination address", Toast.LENGTH_SHORT).show();
            return;
        }
        //If the request passes this will run
        try {
            new DirectionFinder(this, origin, destination).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    /**
     * onMapReady method setting mourneview as position and zoom to 15
     * image on map to be stadiums
     */
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mourneview, 15));
        Markers.add(mMap.addMarker(new MarkerOptions()
                .title("Mourneview")
                .position(mourneview)
                .snippet("Capacity 4,160")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.stadiums))));

        mMap.setMyLocationEnabled(true);

    }


    @Override
    /**
     * Method to find direction when set
     */
    public void onDirectionFinderStart() {
        progressDialog = ProgressDialog.show(this, "Please wait.",
                "Finding direction..!", true);

        Runnable progressRunnable = new Runnable() {

            @Override
            public void run() {
                progressDialog.cancel();
            }
        };

        //Set length of time to 3 secs
        Handler pdCanceller = new Handler();
        pdCanceller.postDelayed(progressRunnable, 3000);

        //If origin markers not null
        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        //If destination markers not null
        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        //Remove the polylines if they are no longer needed
        if (polylinePaths != null) {
            for (Polyline polyline : polylinePaths) {
                polyline.remove();
            }
        }
    }

    @Override
    /**
     * Method  to find directions and set line
     */
    public void onDirectionFinderSuccess(List<Route> routes) {
        progressDialog.dismiss();
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();

        for (Route route : routes) {
            //Move screen to start location with zoom level
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 10));
            //will show distance and duration
            ((TextView) findViewById(R.id.tvDuration)).setText(route.duration.text);
            ((TextView) findViewById(R.id.tvDistance)).setText(route.distance.text);

            //Add marker to start location
            originMarkers.add(mMap.addMarker(new MarkerOptions()
                    .title(route.startAddress)
                    .position(route.startLocation)));
            //Add marker to end location
            destinationMarkers.add(mMap.addMarker(new MarkerOptions()
                    .title(route.endAddress)
                    .position(route.endLocation)));

            //Create polyline and  add latLng coordinates to it
            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.RED).
                    width(15);

            for (int i = 0; i < route.points.size(); i++)
                polylineOptions.add(route.points.get(i));

            //Then add polyline to the map
            polylinePaths.add(mMap.addPolyline(polylineOptions));
        }
    }

    /**
     * determine the users location and the accuracy
     */
    public void onConnected() {
        mLocationRequest = LocationRequest.create();
        //High accuracy to determine users location. Will use a lot of battery
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        //Set length of time between updating users location
        //1000 milliseconds
        mLocationRequest.setInterval(1000);


    }

}

