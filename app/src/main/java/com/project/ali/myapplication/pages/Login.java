package com.project.ali.myapplication.pages;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.project.ali.myapplication.R;
import com.project.ali.myapplication.activities.RegisterActivity;
import com.project.ali.myapplication.helpers.InputValidation;
import com.project.ali.myapplication.sql.DatabaseHelper;
//import com.project.ali.myapplication.app.AppConfig;
//import com.project.ali.myapplication.app.AppController;
//import com.project.ali.myapplication.helper.SQLiteHandler;
//import com.project.ali.myapplication.helper.SessionManager;

import org.json.JSONArray;

/**
 * Created by Alistair.
 * * Original code and some code taken from tutorial to connect to db
 * This page is now redundant as the login activity page is used for the app and SQLite
 * HOWEVER the reader can view my various attempts at different connections and the original code
 */
public class Login extends AppCompatActivity implements View.OnClickListener {
    private final AppCompatActivity activity = Login.this;

//    //Progress Dialog
//    private ProgressDialog pDialog;
//
//    //JSON Parser object
//    JSONParser jParser = new JSONParser();
//
//    ArrayList<HashMap<String,String>> login;
//
//    //url to get login details
//    private static String url_login="HTTP";
//
//    //JSON node names
//    private static final String TAG_SUCCESS="success";
//    private static final String TAG_USERNAME="username";
//    private static final String TAG_PASSWORD="password";

    //login JSONArray
    JSONArray login = null;
private static final String TAG = com.project.ali.myapplication.pages.Login.class.getSimpleName();
    private Button Login;
    private EditText Usernames;
    private EditText Passwords;
    private InputValidation inputValidation;
    private DatabaseHelper databaseHelper;
    private ProgressDialog pDialog;

   // private SessionManager session;
  //  private SQLiteHandler db;


    //declaring and initialising the counter
    int counter = 3;

    //Main method for Login
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Setting the screen as the login screen
        setContentView(R.layout.login_screen);

        //calling the validation method
//        validation();

        //forgotpassword
        forgotPasswordClick();

        //back button
        backButtonClick();

        //register click()
        registerClick();





        //Load details
        //new LoadDetails().exceute();

        Login = (Button) findViewById(R.id.LoginNowLogin);
        Usernames = (EditText) findViewById(R.id.edit_username);
        Passwords = (EditText) findViewById(R.id.edit_password);

        //progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        //SQL db handler
     //   db = new SQLiteHandler(getApplicationContext());

        //session manager
       // session = new SessionManager(getApplicationContext());

        //check if user is already logged in or not
        //if (session.isLoggedIn()){
            //User is already logged in. Take to home page
          //  Intent intent = new Intent(Login.this, HomeFragment.class);
            //startActivity(intent);
           // finish();
//        }

        //Login button click event
//        Login.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View view){
//                String Username = Usernames.getText().toString().trim();
//                String Password = Passwords.getText().toString().trim();

                //check for empty data in the form
//                if (!Username.isEmpty() && !Password.isEmpty()){
//                    //login user
//                    checkLogin(Username,Password);
//                }else{
//                    //prompt user for credentials
//                    Toast.makeText(getApplicationContext(), "Please enter details", Toast.LENGTH_LONG)
//                            .show();
//                }
            }
  //      });

    //}
    /**
     * Function to verify login details in db
     */
//    private void checkLogin(final String Usernames, final String Passwords){
//        //tag used to cancel request
//        String tag_string_req ="req_login";
//
//        pDialog.setMessage("Logging in..");
//        showDialog();
//
//        StringRequest strReq = new StringRequest(Request.Method.POST, AppConfig.URL_LOGIN, new Response.Listener<String>() {
//
//            @Override
//            public void onResponse(String response) {
//                Log.d(TAG, "Login response" + response.toString());
//                hideDialog();
//
//                try{
//                    JSONObject jObj = new JSONObject(response);
//                    boolean error= jObj.getBoolean("error");
//
//                    //check for error node in json
//                    if(!error) {
//                        //user succesfully logged in
//                        //create login session
//                        session.setLogin(true);
//
//                        //now store the user in SQLite
//                        JSONObject user = jObj.getJSONObject("User");
//                        String fName = user.getString("FirstName");
//                        String lName = user.getString("Lastname");
//                        String Username = user.getString("Username");
//                        String Password = user.getString("Password");
//
//
//                        //inserting row into users table
//                        db.addUser(fName,lName,Username, Password);
//
//                        //launch home
//                        Intent intent = new Intent(Login.this, HomeFragment.class);
//                        startActivity(intent);
//                        finish();
//                    }else{
//                        //error in login. get the error message
//                        String errors = jObj.getString("error msg");
//                        Toast.makeText(getApplicationContext(),errors, Toast.LENGTH_LONG).show();
//
//                    }
//                }catch (JSONException e ){
//                    e.printStackTrace();
//                    Toast.makeText(getApplicationContext(), "json error" + e.getMessage(), Toast.LENGTH_LONG).show();
//                }
//
//            }
//        }, new Response.ErrorListener(){
//            @Override
//        public void onErrorResponse(VolleyError error){
//                Log.e(TAG, "Login error" + error.getMessage());
//                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
//                hideDialog();
//            }
//
//        }) {
//            @Override
//        protected Map<String, String> getParams(){
//                //posting params to login url
//                Map<String, String>params = new HashMap<String,String>();
//                params.put("Username", Usernames);
//                params.put("Password", Passwords);
//                return params;
//            }
//        };
//        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
//    }

    private void showDialog(){
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog(){
        if(pDialog.isShowing())
            pDialog.dismiss();
    }

    /**
     * Overrule back button on Android so when logged out cannot bypass Login
     */
    @Override
    public void onBackPressed() {

    }




    /**
     * a method to deal with the validation of the username and password
     */
    public void validation() {

        //creating the button object
        final Button sButton = (Button) findViewById(R.id.LoginNowLogin);

        //recognising a button click
        sButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditText username = (EditText) findViewById(R.id.edit_username);
                EditText password = (EditText) findViewById(R.id.edit_password);


        //        if statement to validate the correct username and password comnination
                if (username.getText().toString().equals("userAdmin") && password.getText().toString().equals("userAdmin")) {

                    //toast to show the successful login message to the user
                    Toast.makeText(getApplicationContext(), "Log in successful", Toast.LENGTH_SHORT).show();



               //     toast to show the successful login message to the user
                    Toast.makeText(getApplicationContext(), "Log in successful", Toast.LENGTH_SHORT).show();





                    //else statement that calls the countDown method if the username and password combination is not validated
                } else {
                    countDown();

                }

            }

        });
    }



    private void countDown() {

        //creating the button object
        final Button sButton = (Button) findViewById(R.id.LoginNowLogin);


        TextView attempts = (TextView) findViewById(R.id.NoOfAttemptsLeftNumberLogin);


        //decrementing the counter, used to count down the number of attempts left
        --counter;
        Toast.makeText(getApplicationContext(), "Wrong username or password", Toast.LENGTH_SHORT).show();

        //switch statement to change the number of attempts left and alter the colour of the text
        switch (counter) {
            case 2:
                attempts.setText("2");

                break;

            case 1:
                attempts.setText("1");

                break;

            //default-displays toast that there are no remaining attempts and changes the count to zero
            default:
                attempts.setText("0");
                Toast.makeText(getApplicationContext(), "You have no more attempts", Toast.LENGTH_LONG).show();


                //disables the login now button
                sButton.setEnabled(false);

        }

    }

    /**
     * Method taking user to forgot password page
     */
    public void forgotPasswordClick() {

        TextView forgot_password = (TextView) findViewById(R.id.forgot_password);

        forgot_password.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent forgot_passwordIntent = new Intent(Login.this, ResetPassword.class);
                startActivity(forgot_passwordIntent);
            }
        });
    }

    /**
     * Method taking user back
     */
    public void backButtonClick() {
        //Back button and setting a click listener
        Button BackBtn = (Button) findViewById(R.id.BackButtonLogin);
        BackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), Welcome.class);
                startActivity(i);
            }
        });
    }

    /**
     * Method taking user to register page
     */
        public void registerClick(){
        TextView register = (TextView) findViewById(R.id.RegisterLogin);

        register.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent forgot_passwordIntent = new Intent(Login.this, RegisterActivity.class);
                startActivity(forgot_passwordIntent);
            }
        });
    }

    /**
     * This implemented method is to listen the click on view
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.appCompatButtonLogin:

                validation();
                break;
            case R.id.textViewLinkRegister:
                // Navigate to RegisterActivity
                Intent intentRegister = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(intentRegister);
                break;
        }
    }



}

