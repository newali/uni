package com.project.ali.myapplication.pages;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.project.ali.myapplication.R;

/**
 * Created by Alistair
 */
public class PastChampions extends AppCompatActivity {
    ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.past_champions);


        // Get ListView object from xml
        listView = (ListView) findViewById(R.id.listPast);

        // Values which will be shown on the xml file
        String[] values = new String[]{
                "2002: Germany",
                "2003: France",
                "2004: Spain",
                "2005: Russia",
                "2006: Germany",
                "2007: Germany",
                "2008: Italy",
                "2009: England",
                "2010: France",
                "2011: Germany",
                "2012: Sweden",
                "2013: France",
                "2014: Netherlands",
                "2015: Sweden",
                "2016: France"

        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);


        // Assign adapter to ListView
        listView.setAdapter(adapter);

        // ListView Item Click Listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition = position;

                // ListView Clicked item value
                String itemValue = (String) listView.getItemAtPosition(position);

                // Show Alert
                Toast.makeText(getApplicationContext(),
                        itemValue, Toast.LENGTH_SHORT)
                        .show();


                //Call the eurosport method
                eurosportClick();

                //Call the adidas method
                adidasClick();

                //Call ni method
                Northern();


            }

            /**
             * Method to take user to Northern Ireland webpage
             */
            public void Northern() {
                ImageView reviews = (ImageView) findViewById(R.id.NorthernIrelandCrestBottomBanner);

                reviews.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        Intent NI = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.irishfa.com/"));
                        startActivity(NI);
                    }
                });
            }

            //Method to take the user to Eurosport
            public void eurosportClick() {

                ImageView reviews = (ImageView) findViewById(R.id.EuroSportImage);

                reviews.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        Intent eurosport = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.eurosport.co.uk/"));
                        startActivity(eurosport);
                    }
                });
            }

            //Method to take the user to Adidas
            public void adidasClick() {

                ImageView reviews = (ImageView) findViewById(R.id.AddidasImage);

                reviews.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        Intent adidas = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.adidas.co.uk/"));
                        startActivity(adidas);
                    }
                });
            }

        });
    }
}

