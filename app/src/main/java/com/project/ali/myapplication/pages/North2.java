package com.project.ali.myapplication.pages;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.project.ali.myapplication.R;

/**
 * Created by Alistair
 */
public class North2 extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.north2);

        //Calling Carrick method
        carrick();

        //Calling shamrock
        shamrock();

        //call limavady
        limavady();

        //Call the eurosport method
        eurosportClick();

        //Call the adidas method
        adidasClick();

        //call the ni method
        Northern();
    }


    /**
     * Method to take the user to Eurosport
     */
    public void eurosportClick() {

        ImageView reviews = (ImageView) findViewById(R.id.EuroSportImage);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent eurosport = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.eurosport.co.uk/"));
                startActivity(eurosport);
            }
        });
    }


    /**
     * Method to take user to Northern Ireland webpage
     */
    public void Northern() {
        ImageView reviews = (ImageView) findViewById(R.id.NorthernIrelandCrestBottomBanner);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent NI = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.irishfa.com/"));
                startActivity(NI);
            }
        });
    }

    /**
     * Method to take the user to Adidas
     */
    public void adidasClick() {

        ImageView reviews = (ImageView) findViewById(R.id.AddidasImage);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent adidas = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.adidas.co.uk/"));
                startActivity(adidas);
            }
        });
    }

    /**
     * Method to handle click of taking user to Carrick
     */
    public void carrick() {
        ImageView reviews = (ImageView) findViewById(R.id.carickRangersladiesChamp);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //calling the method to open the home page
                Intent arm = new Intent(North2.this, Carrick.class);
                startActivity(arm);
            }
        });
    }



    /**
     * Method to handle click of taking user to shamrock
     */
    public void shamrock() {
        ImageView reviews = (ImageView) findViewById(R.id.shamrockladiesChamp);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //calling the method to open the home page
                Intent sham = new Intent(North2.this, Shamrock.class);
                startActivity(sham);
            }
        });
    }

    /**
     * Method to handle click of taking user to limavady
     */
    public void limavady(){
        ImageView reviews = (ImageView) findViewById(R.id.limavadyLadiesChamp);

        reviews.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //calling the method to open the home page
                Intent lim = new Intent(North2.this, Limavady.class);
                startActivity(lim);
            }
        });
    }
}

