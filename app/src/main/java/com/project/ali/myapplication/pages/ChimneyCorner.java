package com.project.ali.myapplication.pages;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.project.ali.myapplication.R;

/**
 * Created by Alistair
 */
public class ChimneyCorner extends AppCompatActivity {
    ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chimney_corner_ladies);


        //Finding the list from the xml page
        listView = (ListView) findViewById(R.id.listChimney);

        //Array to hold values which will be displayed on the xml page
        String[] values = new String[]{
                "Sponsor: Reds",
                "Home Kit: Red/White",
                "Away Kit: Black",
                "Training: Wed 19:300-21:00",
                "Phone number: 07702455701",
                "Manager name: Donnna"

        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);


        // Listview
        listView.setAdapter(adapter);

        //OnItemClickListener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition = position;

                //Get the value of the item clicked
                String itemValue = (String) listView.getItemAtPosition(position);

                //Toast to show information clicked.
                //No real purpose but to highlight info
                Toast.makeText(getApplicationContext(),
                        itemValue, Toast.LENGTH_SHORT)
                        .show();


                //Call the eurosport method
                eurosportClick();

                //Call the adidas method
                adidasClick();

                //call the ni method
                Northern();

                //Call the chimney fb method
                chimneyFb();
            }


            /**
             * Method to take the user to Eurosport
             */
            public void eurosportClick() {

                ImageView reviews = (ImageView) findViewById(R.id.EuroSportImage);

                reviews.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        Intent eurosport = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.eurosport.co.uk/"));
                        startActivity(eurosport);
                    }
                });
            }


            /**
             * Method to take user to Northern Ireland webpage
             */
            public void Northern() {
                ImageView reviews = (ImageView) findViewById(R.id.NorthernIrelandCrestBottomBanner);

                reviews.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        Intent NI = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.irishfa.com/"));
                        startActivity(NI);
                    }
                });
            }

            /**
             * Method to take the user to Adidas
             */
            public void adidasClick() {

                ImageView reviews = (ImageView) findViewById(R.id.AddidasImage);

                reviews.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        Intent adidas = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.adidas.co.uk/"));
                        startActivity(adidas);
                    }
                });
            }

            /**
             * Method to take user to fb webpage
             */
            public void chimneyFb() {
                ImageView reviews = (ImageView) findViewById(R.id.fbImageChimney);

                reviews.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        Intent NI = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/ChimneyCornerLFC/"));
                        startActivity(NI);
                    }
                });
            }
        });
    }
}

