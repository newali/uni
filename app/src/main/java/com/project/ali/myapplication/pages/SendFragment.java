package com.project.ali.myapplication.pages;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import net.hockeyapp.android.FeedbackManager;
import com.project.ali.myapplication.R;

/**
 * Created by Alistair
 */
public class SendFragment extends Fragment implements View.OnClickListener {
   Button Bug;
   ImageView Link;
    View myView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //instansiate the view
        myView = inflater.inflate(R.layout.send_page, container, false);

        //Call method for bug report
        bug();

        return myView;
    }


    /**
     * bug method to set on click listener
     */
    public void bug(){
        Bug = (Button) myView.findViewById(R.id.BugButton);
        Bug.setOnClickListener(this);

    }

    /**
     * Method to take the user to Northern Ireland page
     */
    public void NIClick() {

        Link = (ImageView) myView.findViewById(R.id.NorthernIrelandCrestBottomBanner);
        Link.setOnClickListener(this);

    }

    /**
     * Method to take the user to Addidas page
     */
    public void AddidasClick() {

        Link = (ImageView) myView.findViewById(R.id.AddidasImage);
        Link.setOnClickListener(this);

    }

    /**
     * Method to take the user to Eurosport page
     */
    public void EurosportClick() {

        Link = (ImageView) myView.findViewById(R.id.EuroSportImage);
        Link.setOnClickListener(this);

    }


    /**
     * Switch for user clicking the bug button
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.BugButton:
                Intent link = new Intent(v.getContext(), BugReport.class);
                startActivity(link);
                break;
            case R.id.AddidasImage:
                Intent addidas = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.adidas.co.uk/"));
                startActivity(addidas);
                break;
            case R.id.EuroSportImage:
                Intent euro = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.eurosport.co.uk/"));
                startActivity(euro);
                break;
            case R.id.NorthernIrelandCrestBottomBanner:
                Intent ni = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.irishfa.com/"));
                startActivity(ni);
                break;
            default:
                break;
        }
    }

    /**
     * Method does not work in a fragment
     */
    public void onBackPressed() {

    }
}
