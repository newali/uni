package com.project.ali.myapplication.pages;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.project.ali.myapplication.R;

import java.io.IOException;
import java.util.List;

/**
 * Created by Alistair
 * Original code and some taken from tutorial
 */
public class ShamrockPark extends AppCompatActivity {
    //Location Request for enabling users Location
    private LocationRequest mLocationRequest;
    View myView;
    //Create GoogleMap object
    private GoogleMap googleMap;
    private static final LatLng Shamrock = new LatLng( 54.413056,-6.457778);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Setting the screen as the login screen
        setContentView(R.layout.shamrock_park_directions);

        //Call method processMap
        processMap();

        //Call method onConnected
        onConnected();

        //Call method Gps
        Gps();


    }

    /**
     * Connect to the fragment map on the xml page
     */
    public void processMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

        }
        //If googlemap is not null run setUpMap method
        if (googleMap != null) {
            setUpMap();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        processMap();
    }

    /**
     * Set up the map on Shamrock park with zoom level
     */
    public void setUpMap(){
        googleMap.addMarker(new MarkerOptions().position(Shamrock).title("Shamrock park"));
        //now zoom camera
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Shamrock, 15));
    }


    /**
     * On click method for change map type in xml class
     */
    public void changeTypeSham(View view){
        //check if map type is normal
        if (googleMap.getMapType()== GoogleMap.MAP_TYPE_NORMAL){
            //set to hybrid
            googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        }else {
            //map type normal
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }

    }


    /**
     * determine the users location and the accuracy
     */
    public void onConnected() {
        mLocationRequest = LocationRequest.create();
        // Enable MyLocation Button in the Map
        googleMap.setMyLocationEnabled(true);
        //High accuracy to determine users location. Will use a lot of battery
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        //Set length of time between updating users location
        //1000 milliseconds
        mLocationRequest.setInterval(1000);


    }

    /**
     * On click method for xml button to search
     * @param view
     */
    public void onSearchSham( View view) {
        //Find the edit text
        EditText Location = (EditText) findViewById(R.id.TFAddressSham);
        String location = Location.getText().toString();

        List<Address> addressList = null;

        LatLng latLng = null;
        if (location != null || !location.equals("")) {
            //Allows user to search any string in form of long or lat
            Geocoder geocoder = new Geocoder(this);
            try {
                //List of objects from geocoder and only one result
                addressList = geocoder.getFromLocationName(location, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Address address = addressList.get(0);
            latLng = new LatLng(address.getLatitude(), address.getLongitude());
            googleMap.addMarker(new MarkerOptions().position(latLng).title("Marker"));
            googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

        }else{
             Toast.makeText(getApplicationContext(),
                    "No search Information entered", Toast.LENGTH_LONG)
                    .show();

        }


    }



    /**
     * Method to take user to directions page
     */
    public void Gps(){
        Button GPS = (Button) findViewById(R.id.btn_request_direction);
        GPS.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent GPSIntent = new Intent(ShamrockPark.this, ShamrockGps.class);
                startActivity(GPSIntent);
            }
        });
    }
}

