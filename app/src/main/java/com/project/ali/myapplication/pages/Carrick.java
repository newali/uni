package com.project.ali.myapplication.pages;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.project.ali.myapplication.R;

/**
 * Created by Alistair
 */
public class Carrick extends AppCompatActivity {
    ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.carrick);

        //Finding the list from the xml page
        listView = (ListView) findViewById(R.id.listCarrick);

        //Array to hold values which will be displayed on the xml page
        String[] values = new String[]{
                "Sponsor: None",
                "Home Kit: Orange",
                "Away Kit: Unknown",
                "Training: Wed 20:30-21:30",
                "Phone number: 079305288529",
                "Manager name: Gemma"

        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);


        // Listview
        listView.setAdapter(adapter);

        //OnItemClickListener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition = position;

                //Get the value of the item clicked
                String itemValue = (String) listView.getItemAtPosition(position);

                //Toast to show information clicked.
                //No real purpose but to highlight info
                Toast.makeText(getApplicationContext(),
                        itemValue, Toast.LENGTH_SHORT)
                        .show();

                //Facebook click
                facebookClick();

                //adidas click
                adidasClick();

                //eurosport click
                eurosportClick();
            }

            /**
             * Method to take the user to facebook
             */
            public void facebookClick() {

                ImageView reviews = (ImageView) findViewById(R.id.facebook_logo);

                reviews.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        Intent facebook = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/UEFAWU192017/?pnref=story"));
                        startActivity(facebook);
                    }
                });
            }

            /**
             * Method to take the user to Adidas
             */
            public void adidasClick() {

                ImageView reviews = (ImageView) findViewById(R.id.AddidasImage);

                reviews.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        Intent adidas = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.adidas.co.uk/"));
                        startActivity(adidas);
                    }
                });
            }


            /**
             * Method to take the user to Eurosport
             */
            public void eurosportClick() {

                ImageView reviews = (ImageView) findViewById(R.id.EuroSportImage);

                reviews.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        Intent eurosport = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.eurosport.co.uk/"));
                        startActivity(eurosport);
                    }
                });
            }


        });
    }
}