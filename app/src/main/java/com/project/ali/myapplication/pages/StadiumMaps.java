package com.project.ali.myapplication.pages;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.project.ali.myapplication.R;


/*
 * Created by Alistair
 */
public class StadiumMaps extends Fragment implements View.OnClickListener{
    ImageView stadium;
    ImageView Link;
    View myView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        myView = inflater.inflate(R.layout.stadium_maps, container, false);


        //Call method stadiums
        Stadiums();

        //Call method shamrock
        shamrock();

        //Call method for mourneview
        mourneview();

        //Call method for showgrounds
        showgrounds();

        //Call NI
        NIClick();

        //Call eurosport
        EurosportClick();

        //Call Addidas
        AddidasClick();



        return myView;
    }


    /**
     * Method for clicking Northern Ireland stadium
     */
    public void Stadiums(){
            stadium = (ImageView) myView.findViewById(R.id.NationalStadium);
            stadium.setOnClickListener(this);
        }
    /**
     * Method for clicking Shamrock park stadium
     */
    public void shamrock(){
        stadium = (ImageView) myView.findViewById(R.id.shamrock_Stadium);
        stadium.setOnClickListener(this);
    }

    /**
     * Method for clicking Mourneview stadium
     */
    public void mourneview(){
        stadium = (ImageView) myView.findViewById(R.id.Mourneview);
        stadium.setOnClickListener(this);
    }

    /**
     * Method for clicking Showgrounds stadium
     */
    public void showgrounds(){
        stadium = (ImageView) myView.findViewById(R.id.showgrounds);
        stadium.setOnClickListener(this);
    }

    /**
     * Method to take the user to Northern Ireland page
     */
    public void NIClick() {

        Link = (ImageView) myView.findViewById(R.id.NorthernIrelandCrestBottomBanner);
        Link.setOnClickListener(this);

    }

    /**
     * Method to take the user to Addidas page
     */
    public void AddidasClick() {

        Link = (ImageView) myView.findViewById(R.id.AddidasImage);
        Link.setOnClickListener(this);

    }

    /**
     * Method to take the user to Eurosport page
     */
    public void EurosportClick() {

        Link = (ImageView) myView.findViewById(R.id.EuroSportImage);
        Link.setOnClickListener(this);

    }



    /**
     * Switch for user clicking an image
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.NationalStadium:
                Intent NI = new Intent(v.getContext(), NationalStadiumDirections.class);
                startActivity(NI);
                break;
            case R.id.shamrock_Stadium:
                Intent shamrock = new Intent(v.getContext(), ShamrockPark.class);
                startActivity(shamrock);
                break;
            case R.id.Mourneview:
                Intent mourneview = new Intent(v.getContext(), MourneviewDirections.class);
                startActivity(mourneview);
                break;
            case R.id.showgrounds:
                Intent Showgrounds = new Intent(v.getContext(), com.project.ali.myapplication.pages.Showgrounds.class);
                startActivity(Showgrounds);
                break;
            case R.id.AddidasImage:
                Intent addidas = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.adidas.co.uk/"));
                startActivity(addidas);
                break;
            case R.id.EuroSportImage:
                Intent euro = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.eurosport.co.uk/"));
                startActivity(euro);
                break;
            case R.id.NorthernIrelandCrestBottomBanner:
                Intent ni = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.irishfa.com/"));
                startActivity(ni);
                break;
            default:
                break;

        }
    }
    public void onBackPressed() {

    }
}