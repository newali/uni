package com.project.ali.myapplication.pages;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.ali.myapplication.R;

/**
 * Created by Alistair
 */
public class NextYearFragment extends Fragment implements View.OnClickListener {
   View myView;
    ImageView Link;
    TextView info;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //instansiate the view
        myView = inflater.inflate(R.layout.next_year, container,false);

        //Call NI method
        NIClick();

        //Call eurosport method
        EurosportClick();

        //Call Addidas method
        AddidasClick();

        //Call moreInfo method
        moreInfo();

        return myView;
    }

    /**
     * moreInfo method setting the on click listener
     */
    public void moreInfo(){
        info= (TextView) myView.findViewById(R.id.MoreInformationLink);
        info.setOnClickListener(this);
    }

    /**
     * Method to take the user to Northern Ireland page
     */
    public void NIClick() {

        Link = (ImageView) myView.findViewById(R.id.NorthernIrelandCrestBottomBanner);
        Link.setOnClickListener(this);

    }

    /**
     * Method to take the user to Addidas page
     */
    public void AddidasClick() {

        Link = (ImageView) myView.findViewById(R.id.AddidasImage);
        Link.setOnClickListener(this);

    }

    /**
     * Method to take the user to Eurosport page
     */
    public void EurosportClick() {

        Link = (ImageView) myView.findViewById(R.id.EuroSportImage);
        Link.setOnClickListener(this);

    }

    /**
     * Switch for user clicking link to more information
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.MoreInformationLink:
                //More info link will need updated to take to correct page when available
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://en.wikipedia.org/wiki/2018_UEFA_Women's_Under-19_Championship"));
                startActivity(i);
                break;
            case R.id.AddidasImage:
                Intent addidas = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.adidas.co.uk/"));
                startActivity(addidas);
                break;
            case R.id.EuroSportImage:
                Intent euro = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.eurosport.co.uk/"));
                startActivity(euro);
                break;
            case R.id.NorthernIrelandCrestBottomBanner:
                Intent ni = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.irishfa.com/"));
                startActivity(ni);
                break;
            default:
                break;
        }
    }

    /**
     * Method will not take effect in fragment class
     */
    public void onBackPressed() {

    }
}
