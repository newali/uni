//package com.project.ali.myapplication.backEnd;
//
//import android.content.ContentValues;
//import android.content.Context;
//import android.database.sqlite.SQLiteDatabase;
//import android.database.sqlite.SQLiteOpenHelper;
//
///**
// * Created by Alistair on 29-Mar-17. Code taken from online source
// */
//public class StoreUser extends SQLiteOpenHelper {
//
//    public StoreUser(Context context) {
//
//        super(context, Setup.DATABASE_NAME, null, 1);
//    }
//
//    // public boolean insertData(String email, String password, String first_name, String last_name){
//    public boolean insertData(String first_name,String last_name, String sex, String email, String username,
//                              String password, String first_time){
//
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues contentValues = new ContentValues();
//
//        //contentValues.put(DBSetup.USER_COL1, email);
//        //contentValues.put(DBSetup.USER_COL2, password);
//        //contentValues.put(DBSetup.USER_COL3, first_name);
//        //contentValues.put(DBSetup.USER_COL4, last_name);
//
//        /* if an error occurs with insert method, -1 is returned
//        *  if the method is executed successfully, the correct value is returned
//        *  by making it a long, we can see if -1 is returned, which will inform us of an error
//        *  set the boolean to false if there is an error
//        */
//        long result = db.insert(Setup.USER_TABLE_NAME, null, contentValues);
//        return result != -1;
//    }
//
//    @Override
//    public void onCreate(SQLiteDatabase db) {
//
//    }
//
//    @Override
//    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//
//    }
//}
//
//
