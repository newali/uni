package com.project.ali.myapplication.pages;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.ali.myapplication.R;

/**
 * Created by Alistair
 */
public class LocalWomensTeamFragment extends Fragment implements View.OnClickListener {

    TextView txt;
    ImageView Link;
    View myView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //instansiate the view
        myView = inflater.inflate(R.layout.local_womens_teams, container, false);


        //Call championship
        championship();

        //Call north1
        north1();

        //call north2
        north2();

        //call south1
        south1();

        //call south2
        south2();

        //Call NI
        NIClick();

        //Call eurosport
        EurosportClick();

        //Call Addidas
        AddidasClick();

        return myView;
    }


    /**
     * Championship method will be called in switch
     */
    public void championship(){
        txt = (TextView) myView.findViewById(R.id.ChampionshipLocal);
        txt.setOnClickListener(this);

    }

    /**
     * North2 method will be called in switch
     */
    public  void north1(){
        TextView txt = (TextView) myView.findViewById(R.id.North1Local);
        txt.setOnClickListener(this);
    }

    /**
     * North2 method will be called in switch
     */
    public void north2(){
    txt = (TextView) myView.findViewById(R.id.North2Local);
    txt.setOnClickListener(this);
}

    /**
     * South1 method will be called in switch
     */
    public  void south1(){
        TextView txt = (TextView) myView.findViewById(R.id.South1Local);
        txt.setOnClickListener(this);
    }


    /**
     * Method south2 will be called in switch
     */
    public void south2(){
        TextView txt = (TextView) myView.findViewById(R.id.South2Local);
        txt.setOnClickListener(this);
    }



    /**
     * Method to take the user to Northern Ireland page
     */
    public void NIClick() {

        Link = (ImageView) myView.findViewById(R.id.NorthernIrelandCrestLocalTeams);
        Link.setOnClickListener(this);

    }

    /**
     * Method to take the user to Addidas page
     */
    public void AddidasClick() {

        Link = (ImageView) myView.findViewById(R.id.AdidasLocalWomens);
        Link.setOnClickListener(this);

    }

    /**
     * Method to take the user to Eurosport page
     */
    public void EurosportClick() {

        Link = (ImageView) myView.findViewById(R.id.EurosportLocalWomens);
        Link.setOnClickListener(this);

    }

    /**
     * Switch for user clicking a button
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.AdidasLocalWomens:
                Intent addidas = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.adidas.co.uk/"));
                startActivity(addidas);
                break;
            case R.id.EurosportLocalWomens:
                Intent euro = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.eurosport.co.uk/"));
                startActivity(euro);
                break;
            case R.id.NorthernIrelandCrestLocalTeams:
                Intent ni = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.irishfa.com/"));
                startActivity(ni);
                break;
            case R.id.ChampionshipLocal:
                Intent champ = new Intent(v.getContext(), Championship.class);
                startActivity(champ);
                break;
            case R.id.North1Local:
                Intent North = new Intent(v.getContext(), North.class);
                startActivity(North);
                break;
            case R.id.North2Local:
                Intent North2 = new Intent(v.getContext(), North2.class);
                startActivity(North2);
                break;
            case R.id.South1Local:
                Intent South = new Intent(v.getContext(), South.class);
                startActivity(South);
                break;
            case R.id.South2Local:
                Intent South2 = new Intent(v.getContext(), South2.class);
                startActivity(South2);
                break;
            default:
                break;
        }
    }

    /**
     * back button pressed
     */
    public void onBackPressed() {

    }
}
