package com.project.ali.myapplication.pages;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.project.ali.myapplication.R;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import twitter4j.HttpResponse;

/**
 * Created by Alistair
 */
public class NITeamInfo extends AppCompatActivity {
    ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.northern_ireland_team_information);


        //Finding the list from the xml page
        listView = (ListView) findViewById(R.id.listNI);

        //Array to hold values which will be displayed on the xml page
        String[] values = new String[]{
                "Goalkeeper: ",
                "Goalkeeper: ",
                "Defender: ",
                "Defender: ",
                "Defender: ",
                "Defender: ",
                "Defender: ",
                "Midfielder: ",
                "Midfielder: ",
                "Midfielder: ",
                "Midfielder: ",
                "Forwards: ",
                "Forwards: ",
                "Forwards: ",
                "Forwards: ",
                "Forwards: ",
                "Forwards: ",
                "Forwards: "

        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);


        // Listview
        listView.setAdapter(adapter);

        //OnItemClickListener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition = position;

                //Get the value of the item clicked
                String itemValue = (String) listView.getItemAtPosition(position);

                //Toast to show information clicked.
                //No real purpose but to highlight info
                Toast.makeText(getApplicationContext(),
                        itemValue, Toast.LENGTH_SHORT)
                        .show();


            }


            //Call return player info method
        //    returnPlayerInfo();
        });
    }


        /**
         * return player Info method
         */

 //   public void returnPlayerInfo() {
//        String url = "http://Sample-env.ueifrtzmbm.us-west-2.elasticbeanstalk.com/NIPlayer.php";
//        DefaultHttpClient httpClient = new DefaultHttpClient();
//
//        HttpGet httpGet = new HttpGet(url);
//
//        HttpResponse httpResponse = httpClient.execute(httpGet);
//
//        httpEntity = httpResponse.getEntity();
    }






