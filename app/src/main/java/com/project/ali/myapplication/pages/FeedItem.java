package com.project.ali.myapplication.pages;

/**
 * Created by Alistair
 * Used in ReadRsss which is defunct
 * Created with the aid of tutorials which can be found in the write up bibliography
 */
public class FeedItem {
    String title;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    String link;
    String description;
    String pubDate;
    String thumbnailUrl;

}
