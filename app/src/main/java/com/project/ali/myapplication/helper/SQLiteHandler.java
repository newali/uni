//package com.project.ali.myapplication.helper;
//
//import android.content.ContentValues;
//import android.content.Context;
//import android.database.Cursor;
//import android.database.sqlite.SQLiteDatabase;
//import android.database.sqlite.SQLiteOpenHelper;
//import android.util.Log;
//
//import java.util.HashMap;
//
///**
// * Taken from online source
// * Ravi Tamada
// * Android login and registration with php, mysql and sqlite
// * www.androidhive.info/2012/01/android-login-and-registration-with-php-mysql-and-sqlite/
// */
//public class SQLiteHandler extends SQLiteOpenHelper {
//    private static final String TAG = SQLiteHandler.class.getSimpleName();
//
//    //All static vars
//    //database version
//    private static final int DATABASE_VERSION=1;
//    //Database name
//    private static final String DATABASE_NAME="android_api";
//
//    //login table name
//    private static final String TABLE_USER= "User";
//
//    //Login table column names
//    private static final String KEY_FNAME="fName";
//   private static final String KEY_LNAME="lName";
////    private static final String KEY_SEX="Sex";
////    private static final String KEY_EMAIL="Email";
//    private static final String KEY_USERNAME="Username";
//    private static final String KEY_PASSWORD="Password";
//
//    public SQLiteHandler(Context context){
//        super(context, DATABASE_NAME, null, DATABASE_VERSION);
//    }
//    //Creating tables
//    @Override
//    public void onCreate(SQLiteDatabase db){
//        String CREATE_LOGIN_TABLE = "CREATE TABLE" + TABLE_USER + "("
//                +KEY_USERNAME + "TEXT PRIMARY KEY," + KEY_PASSWORD + " TEXT" +KEY_FNAME + "TEXT" + KEY_LNAME + "TEXT" + ")";
//        db.execSQL(CREATE_LOGIN_TABLE);
//
//        Log.d(TAG, "Database tables created");
//    }
//
//    //Upgrading database
//    @Override
//    public void onUpgrade(SQLiteDatabase db, int OldVersion, int newVersion){
//        //Drop older table if exists
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
//
//        //Create tables again
//        onCreate(db);
//    }
//
//    /**
//     * Storing user details in database
//     */
//    public void addUser(String fName, String lName, String Username, String Password){
//        SQLiteDatabase db= this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put(KEY_FNAME, fName);
//        values.put(KEY_LNAME, lName);
//        values.put(KEY_USERNAME, Username);
//        values.put(KEY_PASSWORD, Password);
//
//        //Inserting row
//        long username = db.insert(TABLE_USER, null, values);
//        db.close();// closing db connection
//
//        Log.d(TAG, "New user inserted");
//    }
//
//    /*
//    *Getting user info from db
//     */
//    public HashMap<String, String>getUserDetails(){
//        HashMap<String,String> User = new HashMap<String, String>();
//        String selectQuery = "SELECT * FROM "+TABLE_USER;
//
//        SQLiteDatabase db  = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//        //move to first row
//        cursor.moveToFirst();
//        if(cursor.getCount()>0){
//            User.put("Username", cursor.getString(1));
//            User.put("Password", cursor.getString(2));
//        }
//        cursor.close();
//        db.close();
//        //return user
//        Log.d(TAG, "Fetching user from sqlite" + User.toString());
//        return
//                 User;
//    }
//    /**
//     * Recreate db delete all tables and create them again
//     */
//    public void deleteUsers(){
//        SQLiteDatabase db = this.getWritableDatabase();
//        //delete all rows
//        db.delete(TABLE_USER, null, null);
//        db.close();
//        Log.d(TAG, "DELETED ALL USERS INFO");
//    }
//}
