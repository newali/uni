package com.project.ali.myapplication.pages;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.project.ali.myapplication.R;
import com.project.ali.myapplication.activities.LoginActivity;


/**
 * Created by Alistair
 */
public class LogoutFragment extends Fragment implements  View.OnClickListener {
    View myView;
    Button logout;
    ImageView sponsor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //instansiate the view
        myView = inflater.inflate(R.layout.logout_screen, container, false);

        //Call the logout method
        LogoutClick();

        //Call adidasClick
        AddidasClick();

        //Call northernIrelandClick
        NorthernIrelandClick();

        //Call EuroSportClick
        EuroSportClick();

        return myView;
    }

    /**
     * Method to logout the user
     */
    public void LogoutClick() {
        logout = (Button) myView.findViewById(R.id.LogoutButton);
        logout.setOnClickListener(this);
    }

    /**
     * Method to take user to addidas
     */
    public void AddidasClick(){
        sponsor = (ImageView) myView.findViewById(R.id.AddidasImage);
        sponsor.setOnClickListener(this);
    }

    /**
     * Method to take user to addidas
     */
    public void NorthernIrelandClick(){
        sponsor = (ImageView) myView.findViewById(R.id.NorthernIrelandCrestBottomBanner);
        sponsor.setOnClickListener(this);
    }

    /**
     *  Method to take user to addidas
     */
       public void EuroSportClick(){
        sponsor = (ImageView) myView.findViewById(R.id.EuroSportImage);
        sponsor.setOnClickListener(this);
    }

    /**
     * Switch for user clicking an image or logout button
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.LogoutButton:
                Intent logouts = new Intent(v.getContext(), LoginActivity.class);
                startActivity(logouts);
                break;
            case R.id.AddidasImage:
                Intent Addidas = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.adidas.co.uk/"));
                startActivity(Addidas);
                break;
            case R.id.NorthernIrelandCrestBottomBanner:
                Intent NICrest = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.irishfa.com/"));
                startActivity(NICrest);
                break;
            case R.id.EuroSportImage:
                Intent EuroSport= new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.eurosport.co.uk/"));
                startActivity(EuroSport);
                break;
            default:
                break;
        }
    }
}