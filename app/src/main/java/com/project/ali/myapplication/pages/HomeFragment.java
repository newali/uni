package com.project.ali.myapplication.pages;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import com.project.ali.myapplication.R;



/**
 * Created by Alistair
 */
public class HomeFragment extends Fragment implements View.OnClickListener {
    Intent intent;
    View myView;
    ImageView reviews;
    ImageView sponsor;
    Button scores;



        @Nullable
        @Override
        public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle
        savedInstanceState) {
            //instansiate the view
            myView = inflater.inflate(R.layout.home_screen, container, false);



        //Call the Facebook method
        FacebookClick();

        //Call the twitter method
        TwitterClick();

        //Call the snapchat method
        SnapchatClick();

        // Call the youtube method
        YoutubeClick();

        // Call the instagram method
        InstagramClick();

        //Call the email method
        EmailClick();

        //Call the sponsors methods
        AddidasClick();
        NorthernIrelandClick();
        EuroSportClick();
        LiveScoreClick();

        return myView;
    }


    /**
     *  Method to take the user to Facebook
     */
    public void FacebookClick() {
        reviews = (ImageView) myView.findViewById(R.id.facebook_logo);
        reviews.setOnClickListener(this);
    }

    /*
    *Method to take user to twitter
     */
    public void TwitterClick(){
        reviews = (ImageView) myView.findViewById(R.id.twitter_logo);
        reviews.setOnClickListener(this);
    }

    /**
     *Method to take user to youtube
     */
    public void YoutubeClick(){
        reviews = (ImageView) myView.findViewById(R.id.youtube_logo);
        reviews.setOnClickListener(this);
    }

    /**
     * Method to take user to snapchat
     */
    public void SnapchatClick(){
        reviews = (ImageView) myView.findViewById(R.id.snapchat_logo);
        reviews.setOnClickListener(this);
    }

    /**
     * Method to take user to instagram
     */
    public void InstagramClick(){
        reviews = (ImageView) myView.findViewById(R.id.instagram_logo);
        reviews.setOnClickListener(this);
    }

    /**
     *  Method to take user to email
     */
    public void EmailClick(){
        reviews = (ImageView) myView.findViewById(R.id.email_logo);
        reviews.setOnClickListener(this);
    }

    /**
     * Method to take user to addidas
     */
    public void AddidasClick(){
        sponsor = (ImageView) myView.findViewById(R.id.AddidasImage);
        sponsor.setOnClickListener(this);
    }

    /**
     *  Method to take user to addidas
     */
    public void NorthernIrelandClick(){
        sponsor = (ImageView) myView.findViewById(R.id.NorthernIrelandCrestBottomBanner);
        sponsor.setOnClickListener(this);
    }

    /**
     *  Method to take user to addidas
     */
    public void EuroSportClick(){
        sponsor = (ImageView) myView.findViewById(R.id.EuroSportImage);
        sponsor.setOnClickListener(this);
    }

    /**
     * Method to take user to live scores
     */
    public void LiveScoreClick(){
        scores = (Button) myView.findViewById(R.id.HomeScreenLiveScoreFeed);
        scores.setOnClickListener(this);
    }


    /**
     * Switch for user clicking an image and taking them to the set website
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.facebook_logo:
                Intent facebook = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/UEFAWU192017/?pnref=story"));
                startActivity(facebook);
                break;
            case R.id.twitter_logo:
                Intent twitter = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/OfficialIrishFA?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor"));
                startActivity(twitter);
                break;
            case R.id.snapchat_logo:
                Intent snapchat = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.snapchat.com/l/en-gb/"));
                startActivity(snapchat);
                break;
            case R.id.youtube_logo:
                Intent youtube = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/user/OfficialIrishFA"));
                startActivity(youtube);
                break;
            case R.id.email_logo:
                Intent email = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:U19Euros@outlook.com"));
                startActivity(email);
                break;
            case R.id.instagram_logo:
                Intent instagram = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/?hl=en"));
                startActivity(instagram);
                break;
            case R.id.AddidasImage:
                Intent Addidas = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.adidas.co.uk/"));
                startActivity(Addidas);
                break;
            case R.id.NorthernIrelandCrestBottomBanner:
                Intent NICrest = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.irishfa.com/"));
                startActivity(NICrest);
                break;
            case R.id.EuroSportImage:
                Intent EuroSport= new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.eurosport.co.uk/"));
                startActivity(EuroSport);
                break;
            case R.id.HomeScreenLiveScoreFeed:
                Intent Score = new Intent(v.getContext(), LiveScores.class);
                startActivity(Score);
            default:
                break;
        }

    }



}
