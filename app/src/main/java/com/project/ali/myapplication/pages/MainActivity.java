package com.project.ali.myapplication.pages;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;


import com.project.ali.myapplication.R;
import com.project.ali.myapplication.pages.AboutFragment;
import com.project.ali.myapplication.pages.DirectionsFragment;
import com.project.ali.myapplication.pages.EuropeanReachFragment;
import com.project.ali.myapplication.pages.FanzoneFragment;
import com.project.ali.myapplication.pages.FixturesFragment;
import com.project.ali.myapplication.pages.GalleryFragment;
import com.project.ali.myapplication.pages.HistoryFragment;
import com.project.ali.myapplication.pages.HomeFragment;
import com.project.ali.myapplication.pages.IFAMessagesFragment;
import com.project.ali.myapplication.pages.LocalWomensTeamFragment;
import com.project.ali.myapplication.pages.LogoutFragment;
import com.project.ali.myapplication.pages.NextYearFragment;
import com.project.ali.myapplication.pages.SendFragment;
import com.project.ali.myapplication.pages.SightsInNiFragment;
import com.project.ali.myapplication.pages.StadiumMaps;
import com.project.ali.myapplication.pages.TeamInformationFragment;
import com.project.ali.myapplication.pages.TicketsFragment;


/**
 * Created by Alistair
 * The Main Activity
 */
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Setting the content layout to activity main
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
                //Setting the drawer layout
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FragmentManager fragmentManagr = getFragmentManager();
        fragmentManagr.beginTransaction().replace(R.id.content_frame, new HomeFragment()).commit();


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        FragmentManager fragmentManager = getFragmentManager();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, new AboutFragment())
                    .commit();
        }
        return super.onOptionsItemSelected(item);
    }




    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    /**
     * Setting the links in the navigation bar and the pages they link to
     */
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        //create fragment manager object for the navigation drop down
        FragmentManager fragmentManager = getFragmentManager();


        if (id == R.id.nav_home) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, new HomeFragment())
                    .commit();
        }else if(id == R.id.nav_Directions) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, new DirectionsFragment())
                    .commit();
    }else if (id == R.id.nav_european) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, new EuropeanReachFragment())
                    .commit();
        } else if (id == R.id.nav_fanzone) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, new FanzoneFragment())
                    .commit();
        } else if (id == R.id.nav_fixtures) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, new FixturesFragment())
                    .commit();
        } else if (id == R.id.nav_local_teams) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, new LocalWomensTeamFragment())
                    .commit();
        } else if (id == R.id.nav_map) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, new StadiumMaps())
                    .commit();
        } else if (id == R.id.nav_teaminfo) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, new TeamInformationFragment())
                    .commit();
        } else if (id == R.id.nav_tickets) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, new TicketsFragment())
                    .commit();
        } else if (id == R.id.nav_sights) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, new SightsInNiFragment())
                    .commit();
        } else if (id == R.id.nav_history) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, new HistoryFragment())
                    .commit();
        } else if (id == R.id.nav_next_year) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, new NextYearFragment())
                    .commit();
       } else if (id == R.id.nav_gallery) {
           fragmentManager.beginTransaction()
                   .replace(R.id.content_frame, new GalleryFragment())
                   .commit();
        } else if (id == R.id.nav_ifa_messages) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, new IFAMessagesFragment())
                    .commit();
        } else if (id == R.id.nav_logout) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, new LogoutFragment())
                    .commit();
        } else if (id == R.id.nav_send) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, new SendFragment())
                    .commit();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }




}
