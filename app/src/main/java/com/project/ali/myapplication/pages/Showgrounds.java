package com.project.ali.myapplication.pages;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.project.ali.myapplication.R;

import java.io.IOException;
import java.util.List;

/**
 * Created by Alistair
 */
public class Showgrounds extends AppCompatActivity {
    LocationRequest mLocationRequest;
    View myView;
    //Create GoogleMap object
    private GoogleMap googleMap;
    private static final LatLng Showgrounds = new LatLng(54.87010,-6.26507);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Setting the screen as the login screen
        setContentView(R.layout.showgrounds);

        //Call method processMap
        processMap();

        //Call method Gps
        Gps();

        //Call method onConnected
        onConnected();

    }

    /**
     * Method to set up map and findFragmentbyId
     */
    public void processMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

        }
        if (googleMap != null) {
            setUpMap();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        processMap();
    }

    /**
     * to set down marker on the stadium and have the capacity
     */
    public void setUpMap(){
        googleMap.addMarker(new MarkerOptions().position(Showgrounds).title("Showgrounds")
                .snippet("Capacity: 4,100")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.stadiums)));
        //now zoom camera
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Showgrounds, 15));
    }

    /**
     * On click method for change map type in xml class
     */
    public void changeType(View view){
        //check if map type is normal
        if (googleMap.getMapType()== GoogleMap.MAP_TYPE_NORMAL){
            //set to hybrid
            googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        }else {
            //map type normal
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }

    }


    /**
     * determine the users location and the accuracy
     */
    public void onConnected() {
        mLocationRequest = LocationRequest.create();
        // Enable MyLocation Button in the Map
        googleMap.setMyLocationEnabled(true);
        //High accuracy to determine users location. Will use a lot of battery
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        //Set length of time between updating users location
        //1000 milliseconds
        mLocationRequest.setInterval(1000);


    }


    /**
     * On click method for xml button to search
     * @param view
     */
    public void onSearchShow( View view) {
        //Find the edit text
        EditText Location = (EditText) findViewById(R.id.TFAddressShow);
        String location = Location.getText().toString();

        List<Address> addressList = null;


        LatLng latLng = null;
        if (location != null || !location.equals("")) {
            //Allows user to search any string in form of long or lat
            Geocoder geocoder = new Geocoder(this);
            try {
                //List of objects from geocoder and only one result
                addressList = geocoder.getFromLocationName(location, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Address address = addressList.get(0);
            latLng = new LatLng(address.getLatitude(), address.getLongitude());
            googleMap.addMarker(new MarkerOptions().position(latLng).title("Marker"));
            googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

        }else{
            return;
        }


    }


    /**
     * Method to take user to directions page
     */
    public void Gps(){
        Button GPS = (Button) findViewById(R.id.btn_request_direction);
        GPS.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent GPSIntent = new Intent(Showgrounds.this, ShowgroundGps.class);
                startActivity(GPSIntent);
            }
        });
    }
}

