package com.project.ali.myapplication.pages;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.project.ali.myapplication.R;

/**
 * Created by Alistair
 */
public class SpainTeamInfo extends AppCompatActivity {
    ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spain_team_information);


        //Finding the list from the xml page
        listView = (ListView) findViewById(R.id.listSpain);

        //Array to hold values which will be displayed on the xml page
        String[] values = new String[]{
                "Goalkeeper: M.Rodriguez",
                "Goalkeeper: M.Sampalo",
                "Defender: S.Merida",
                "Defender: L.Bravo",
                "Defender: A.Sierra",
                "Defender: C.Hernandez",
                "Defender: C.Montagut",
                "Midfielder: Y.Albalat",
                "Midfielder: P.Guijarro",
                "Midfielder: A.Bonmati",
                "Midfielder: M.Oroz",
                "Forwards: M.Portales",
                "Forwards: C.Menayo",
                "Forwards: N.Montilla",
                "Forwards: S.Rubio",
                "Forwards: L.M.Perez",
                "Forwards: L.Garcia",
                "Forwards: A.Azcona"

        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);


        // Listview
        listView.setAdapter(adapter);

        //OnItemClickListener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition = position;

                //Get the value of the item clicked
                String itemValue = (String) listView.getItemAtPosition(position);

                //Toast to show information clicked.
                //No real purpose but to highlight info
                Toast.makeText(getApplicationContext(),
                        itemValue, Toast.LENGTH_SHORT)
                        .show();

                //Call method for the statistics link
                Stats();
            }

            /**
             * Statistics method to link to webpage
             */
            public void Stats() {

                TextView stat = (TextView) findViewById(R.id.StatisticsLink);

                stat.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        Intent instagram = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.uefa.com/womensunder19/season=2017/statistics/index.html"));
                        startActivity(instagram);
                    }
                });
            }


        });
    }
}

