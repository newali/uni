package com.project.ali.myapplication.pages;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.project.ali.myapplication.R;
import com.project.ali.myapplication.pages.GoldenPlayers;
import com.project.ali.myapplication.pages.PastChampions;

/**
 * Created by Alistair
 */
public class SightsInNiFragment extends Fragment implements View.OnClickListener{
    ImageView Link;
   View myView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //instansiate the view
        myView = inflater.inflate(R.layout.sights_in_ni, container,false);

        //Call method for titanic
        titanic();

        //Call method for Causeway
        causeway();

        //Call method for Bushmills
        bushmills();

        //Call method for Ulster American Folk Park
        folkPark();

        //Call method for Londonderry
        londonderry();

        //Call method for Belfast Bus Company
        bus();

        //Call method for booking.com
        booking();

        //Call NI
        NIClick();

        //Call eurosport
        EurosportClick();

        //Call Addidas
        AddidasClick();

        return myView;
    }

    /**
     * Method for titanic
     */
    public void titanic(){
        ImageView Click = (ImageView) myView.findViewById(R.id.NumberOne);
        Click.setOnClickListener(this);
    }

    /**
     * Method for causeway
     */
    public void causeway(){
        ImageView Click = (ImageView) myView.findViewById(R.id.GiantsCauseway);
        Click.setOnClickListener(this);
    }

    /**
     * Method for Bushmills
     */
    public void bushmills(){
        ImageView Click = (ImageView) myView.findViewById(R.id.Bushmills);
        Click.setOnClickListener(this);
    }

    /**
     * Method for Folk Park
     */
    public void folkPark(){
        ImageView Click = (ImageView) myView.findViewById(R.id.FolkPark);
        Click.setOnClickListener(this);
    }

    /**
     * Method for londonderry
     */
    public void londonderry(){
        ImageView Click = (ImageView) myView.findViewById(R.id.Derry);
        Click.setOnClickListener(this);
    }

    /**
     * Method for belfast city bus sightseeing
     */
    public void bus(){
        ImageView Click = (ImageView) myView.findViewById(R.id.CityBus);
        Click.setOnClickListener(this);
    }

    /**
     * Method for booking
     */
    public void booking(){
        ImageView Click = (ImageView) myView.findViewById(R.id.BookingLogo);
        Click.setOnClickListener(this);
    }

    /**
     * Method to take the user to Northern Ireland page
     */
    public void NIClick() {

        Link = (ImageView) myView.findViewById(R.id.NorthernIrelandCrestBottomBanner);
        Link.setOnClickListener(this);

    }

    /**
     * Method to take the user to Addidas page
     */
    public void AddidasClick() {

        Link = (ImageView) myView.findViewById(R.id.AddidasImage);
        Link.setOnClickListener(this);

    }

    /**
     * Method to take the user to Eurosport page
     */
    public void EurosportClick() {

        Link = (ImageView) myView.findViewById(R.id.EuroSportImage);
        Link.setOnClickListener(this);

    }

    /**
     * Switch for user clicking a button
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.NumberOne:
                Intent titanic = new Intent(Intent.ACTION_VIEW, Uri.parse("http://titanicbelfast.com//"));
                startActivity(titanic);
                break;
            case R.id.GiantsCauseway:
                Intent causeway = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.discovernorthernireland.com/Giants-Causeway-Antrim-Northern-Ireland-Bushmills-P2800"));
                startActivity(causeway);
                break;
            case R.id.Bushmills:
                Intent whiskey = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.bushmills.com/distillery/"));
                startActivity(whiskey);
                break;
            case R.id.FolkPark:
                Intent folk = new Intent(Intent.ACTION_VIEW, Uri.parse("http://nmni.com/uafp"));
                startActivity(folk);
                break;
            case R.id.Derry:
                Intent city = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.discovernorthernireland.com/walledcity/"));
                startActivity(city);
                break;
            case R.id.CityBus:
                Intent bus = new Intent(Intent.ACTION_VIEW, Uri.parse("https://belfastcitysightseeing.com/"));
                startActivity(bus);
                break;
            case R.id.BookingLogo:
                Intent booking = new Intent(Intent.ACTION_VIEW, Uri.parse("http://booking.com"));
                startActivity(booking);
                break;
            case R.id.AddidasImage:
                Intent addidas = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.adidas.co.uk/"));
                startActivity(addidas);
                break;
            case R.id.EuroSportImage:
                Intent euro = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.eurosport.co.uk/"));
                startActivity(euro);
                break;
            case R.id.NorthernIrelandCrestBottomBanner:
                Intent ni = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.irishfa.com/"));
                startActivity(ni);
                break;
            default:
                break;
        }
    }


    public void onBackPressed() {

    }

}
