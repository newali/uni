package com.project.ali.myapplication.pages;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.project.ali.myapplication.R;

/**
 * Created by Alistair
 */
public class Knockout extends AppCompatActivity {

    //Main method for knockout
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Setting the screen as the knockout
        setContentView(R.layout.knockout);
    }
}
