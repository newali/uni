package module;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import module.Route;


/**
 * Created by Alistair and from online source
 */
public interface DirectionFinderListener {
    void onDirectionFinderStart();
    void onDirectionFinderSuccess(List<Route> route);
}